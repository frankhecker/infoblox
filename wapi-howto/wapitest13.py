import requests
import json
import csv

# Get IPv4 network objects (up to a max of 10000).
r = requests.get('https://gm.lab.fhecker.com/wapi/v1.0/network',
                 params={'_max_results': str(2000)},
                 auth=('fhecker', 'DavidB.Epileptic1'))
if r.status_code != requests.codes.ok:
    print r.text
    exitmsg = 'Error {} finding networks: {}'
    sys.exit(exitmsg.format(r.status_code, r.reason))
networks = r.json()

# For each network count all used IPv4 addresses (up to a max of 10000).
stats = []
for network in networks:
    # Retrieve used ipv4address objects for this network.
    r = requests.get('https://gm.lab.fhecker.com/wapi/v1.0/ipv4address',
                     params={'network': network['network'],
                             'network_view': network['network_view'],
                             'status': 'USED',
                             '_return_fields': '',
                             '_max_results': str(10000)},
                 auth=('fhecker', 'DavidB.Epileptic1'))
    if r.status_code != requests.codes.ok:
        print r.text
        exitmsg = 'Error {} finding IP addresses: {}'
        sys.exit(exitmsg.format(r.status_code, r.reason))
    ipv4addresses = r.json()

    # Count the number of ipv4address objects and save the count.
    used = len(ipv4addresses)
    prefix = int(network['network'].split('/')[1])
    size = 2**(32-prefix) - 2
    pct_used = round((100. * used) / size, 1)
    stats.append([network['network'],
                  network['network_view'],
                  size,
                  used,
                  pct_used])

# Export the results.
with open('ipam-stats.csv', 'wb') as out_file:
    out_csv = csv.writer(out_file,
                         delimiter=',',
                         quotechar='"',
                         quoting=csv.QUOTE_MINIMAL)
    out_csv.writerow(['Network',
                      'Network View',
                      'Size',
                      'Used',
                      '% Used'])
    for item in sorted(stats):
        out_csv.writerow(item)
