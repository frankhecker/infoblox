import requests
import sys
import json

request_params = {'name': 'host1.lab.fhecker.com'}
r = requests.get('https://gm.lab.fhecker.com/wapi/v1.0/record:host',
                 params=request_params,
                 auth=('fhecker', 'DavidB.Epileptic1'))
print r.text
print

host_ref = r.json()[0]['_ref']
request_data = {'ipv4addrs-': [{'ipv4addr': '192.168.201.17'},
                               {'ipv4addr': '192.168.201.18'}]}
r = requests.put('https://gm.lab.fhecker.com/wapi/v1.0/' + host_ref,
                  data=json.dumps(request_data),
                  auth=('fhecker', 'DavidB.Epileptic1'))
print r.text
print

request_params = {'_return_fields': 'ipv4addrs'}
r = requests.get('https://gm.lab.fhecker.com/wapi/v1.0/' + host_ref,
                 auth=('fhecker', 'DavidB.Epileptic1'))
print r.text
