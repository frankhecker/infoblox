import requests
import sys
import json
data_to_send = {'network': '192.168.100.0/24',
                'network_view': 'default',
                'comment': 'Test network'}
r = requests.post('https://gm.lab.fhecker.com/wapi/v1.0/network',
                  data=data_to_send,
                  auth=('fhecker', 'DavidB.Epileptic1'))
if r.status_code != requests.codes.created:
    errmsg = 'Error {} creating network: {}'
    sys.exit(errmsg.format(r.status_code, r.reason))
net_ref = r.json()
print r.request.headers
if not net_ref:
    sys.exit('network object not returned')
print net_ref
