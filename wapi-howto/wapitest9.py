import requests
import sys
import json

opts = {'network': '192.168.100.0/24',
        'network_view': 'default'}
r = requests.get('https://gm.lab.fhecker.com/wapi/v1.0/network',
                 params=opts,
                 auth=('fhecker', 'DavidB.Epileptic1'))
if r.status_code != requests.codes.ok:
    errmsg = 'Error {} finding network: {}'
    sys.exit(errmsg.format(r.status_code, r.reason))
nets = r.json()
if not nets:
    sys.exit('network not found')
elif len(nets) > 1:
    sys.exit('network not unique')
net_ref = nets[0]['_ref']

r = requests.delete('https://gm.lab.fhecker.com/wapi/v1.0/' + net_ref,
                    auth=('fhecker', 'DavidB.Epileptic1'))
if r.status_code != requests.codes.ok:
    errmsg = 'Error {} deleting network: {}'
    sys.exit(errmsg.format(r.status_code, r.reason))
