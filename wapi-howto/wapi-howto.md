# How to use the Infoblox NIOS Web API

Frank Hecker (fhecker@infobloxfederal.com)

Document Version 0.1 (2015-03-14)

Copyright 2015 Infoblox, Inc. All Rights Reserved.

## Introduction

Infoblox DDI appliances provide two supported APIs by which external
applications can query, add, modify, or delete DNS, DHCP, and IPAM
data. All 5.x and later versions of NIOS support an older Perl-centric
API (the Perl API or PAPI). NIOS 6.6 introduced a new Web API or WAPI,
sometimes also referred to as the RESTful API. (“REST” or
“REpresentational State Transfer” refers to the principles embodied in
web protocols like HTTP.)

The Infoblox NIOS Web API uses HTTP GET, POST, PUT, and DELETE
requests to implement query, add, modify, and delete operations
respectively, with specially-formatted URLs and (in some cases)
submitted form data used to provide the parameters for these
operations. The Web API is similar in design to APIs used with modern
web services such as Twitter, Github, and the like, and can be called
from a variety of cross-platform languages, including not only Perl
but also Python, Ruby, and others, as well as from OS-specific
languages like Microsoft PowerShell.

The operations and data objects for the Web API are documented in the
official Infoblox _WAPI Documentation_ manual. However that document
is intended as a reference manual and for the most part does not
describe how to use the Web API in actual programs. This document
supplements the reference manual and describes how to use the Web API
for a number of common tasks. Its primary target audience is
administrators who are familar with Infoblox concepts and operations
but have a limited programming background.

This document includes code examples in Python and a number of
complete Python scripts that can be used as-is or modified to meet
particular requirements. Python is used because it is a very popular
language, is available on all platforms either as bundled software or
as a free download, and is powerful while at the same time relatively
easy to understand. This document is not intended as a Python
tutorial, but it does contain brief explanations of common Python
constructs for people with a bit of programming experience but
relatively little knowledge of Python.

For more information on setting up a suitable Python environment on
your own system please see the appendix “Installing and using Python”.
Many examples are also presented using the Curl command line utility,
and thus can be tested even if you don’t have Python or a similar
language available to you.

## Exploring the Web API with your browser

Since the Infoblox NIOS Web API is based on standard web protocols
like HTTP you can make simple WAPI requests by using a web browser
like Google Chrome or Mozilla Firefox. (Chrome and Firefox are better
choices for this than Apple Safari or Microsoft Internet Explorer,
since they will correctly display the WAPI output by default.)

Using a browser is also a good way to easily explore the different
types of objects available via the Web API, how they can be searched
for, and what fields they contain. When coupled with the use of Curl
(described below) you can also test out ways to use the Web API to
create new objects or modify or delete existing ones.

### Testing the Web API

To test the Infoblox NIOS Web API on your own Infoblox system use
either the Chrome or Firefox browser and enter the following URL in
the location bar, replacing `gm.example.com` with the fully-qualified
domain name of your grid master:

    https://gm.example.com/wapi/v1.0/networkview

(If you are using a standalone Infoblox appliance then use the FQDN
that you use when accessing the web interface of your appliance. If
you have deployed Infoblox Cloud Platform appliances as grid members
then you can use the FQDN of one of those grid members. The remainder
of this document assumes that you are using a grid and connecting to
the grid master, but the same instructions apply in almost all cases.)

Since this is an HTTPS URL, if your grid master uses a self-signed
SSL/TLS certificate then you may be prompted to approve a connection
to the system, just as you might be if you attempted to connect to the
grid master web UI. Since the URL does not include a userid and
password you will be prompted to provide them, again as you would be
if you tried to login to the web UI. However instead of a login screen
the prompt is provided by the browser itself using a special dialog.
(The authentication method used is HTTP basic authentication; as with
logging in using the standard login screen, the security of your login
credentials is protected by SSL/TLS encryption of the browser/grid
master communications.)

The remainder of the URL (after the FQDN of the grid master) specifies
this as a Web API operation (‘wapi’), specifies the version of the API
we are using (‘v1.0’), and identifies the Infoblox data object or
objects of interest (‘networkview’). Browsers do an HTTP GET operation
when URLs are entered in this way, so for this API operation we are
doing a query.

If you entered a valid userid and password then the browser should
display output that looks like the following, assuming that you have
only one network view defined:

    <list>
      <value type="object">
        <name>default</name>
        <is_default type="boolean">true</is_default>
        <_ref>networkview/ZG5zLw5ldfdvcmtFdmlleyQw:default/true</_ref>
      </value>
    </list>

This output is in Extensible Markup Language or XML format, one of the
two data formats supported by the Web API. (The other format,
JavaScript Object Notation or JSON, is discussed below.) It can be
interpreted as follows:

* The query returns a list of objects, surrounded by the `<list>` and
  `</list>` XML tags. In this case the list contains only one item
  since this system has only one network view. (For an operation that
  returns more than one object see below.)
* Each data object in the list is enclosed within the `<value
  type="object">` and `</value>` tags, and contains several key/value
  pairs that specify information about the object (which in this case
  is a network view).
* In particular the `_ref` field of the object (within the `<_ref>`
  and `<\_ref>` tags) contains the type of the object (‘networkview’),
  a randomly-generated identifier for the object
  (‘ZG5zLw5ldfdvcmtFdmlleyQw’), and a name component
  (‘default/true’). The `_ref` value (formally referred to as an
  _object reference_) thus uniquely identifies one particular object
  out of all the data objects stored in the grid master’s
  database. (This will be important later when we want to modify or
  delete objects.)
* The other fields of the object (`name` and `is_default`) contain
  other information about the object, in this case that the name of
  the network view is ‘default’ and that it is the default network
  view (since it’s the only one).

### Using a browser to retrieve objects

Similar to the way we retrieved the complete list of defined network
views, you can construct URLs to retrieve lists of all defined objects
of a certain type. For example, if you want to retrieve a list of all
defined networks you can enter a URL like

    https://gm.example.com/wapi/v1.0/network

into your browser; this produces output like the following:

    <list>
      <value type="object">
        <network>151.207.0.0/16</network>
        <network_view>default</network_view>
        <_ref>network/ZG5zLm5ldHdvcmskMTUxLjIwNy4wLjAvMTYvMA:151.207.0.0/16/default</_ref>
      </value>
      <value type="object">
        <comment>Home network</comment>
        <network>192.168.201.0/24</network>
        <network_view>default</network_view>
        <_ref>network/ZG5zLm5ldHdvcmskMTkyLjE2OC4yMDEuMC8yNC8w:192.168.201.0/24/default</_ref>
      </value>
      ...
    </list>

As in the previous example, each object returned has a unique object
reference in the `_ref` field. Each network object also contains
fields containing the network address in CIDR format, the network view
within which the network is defined, and the comment associated with
the network. (If no comment was ever specified for a given network
then the corresponding field is not returned. This is an important
point to remember when retrieving values for fields that are optional
for a given object.)

For a complete list of data objects that can be retrieved see the
chapter “Objects” in the official _WAPI Documentation_ reference
manual for the version of the Web API you are using. You can test
whether a given object type can be retrieved from your particular grid
by entering a URL like

    https://gm.example.com/wapi/vn.m/type

where _n.m_ is the latest API version supported for your grid (e.g.,
‘2.0’ for NIOS 7.0) and _type_ is the object type you want to check.

For example, the object type `grid` (which stores various grid
property values) is supported on Web API versions 1.1 or later, with
the URL

    https://gm.example.com/wapi/v1.1/grid

returning a value like

    <list>
      <value type="object">
        <_ref>grid/b25lLmNsdXN0ZXIkMA:Infoblox</_ref>
      </value>
    </list>

(The string ‘Infoblox’ in the object reference is the grid name as
specified when joining new members to the grid.)

However the `grid` object type is not supported in the first version
of the Web API, so using the URL

    https://gm.example.com/wapi/v1.0/grid

returns an error message:

    { "Error": "AdmConProtoError: Illegal object type (grid)",
      "code": "Client.Ibap.Proto",
      "text": "Illegal object type (grid)"
    }

### Using a browser to search for particular objects

Let’s suppose that we are not interested in seeing all network
objects, but are instead interested in returning data for one
particular network, for example the network with address
10.0.0.0/16. We can search for that particular network by entering the
URL

    https://gm.example.com/wapi/v1.0/network?network=10.0.0.0/16

Here the ‘?’ character introduces one or more query strings specifying
how to search and for what. The `network=10.0.0.0/16` query parameter
specifies the field we are searching on (‘network’) and the particular
value of that field we are using as the search criteria
(‘10.0.0.0/16’); the use of the ‘=’ character specifies that we are
looking for an exact match.

(To forestall potential confusion, the string ‘network’ before the ‘?’
character refers to the name of an object type, and the string
‘network’ after the ‘?’ refers to the name of a field. The fact that
the two names are the same is a coincidence.)

If the network 10.0.0.0/16 exists then the browser will display output
like the following:

    <list>
      <value type="object">
        <network>10.0.0.0/16</network>
        <network_view>default</network_view>
        <_ref>network/ZG5zLm5ldHdvcmskMTAuMC4wLjAvMTYvMA:10.0.0.0/16/default</_ref>
      </value>
    </list>

The format of the object is exactly the same as when we retrieved a
list of all networks, but only one object is returned in the results
list.

If there is no network 10.0.0.0/16 defined then the browser’s output
will instead look as follows:

    </list>

This means that no results were returned, i.e., an empty list. Any
program using the API to search for objects has to have code to deal
with the case when no objects are returned.

It is also possible that a search might return more than one
object. For example, if you have multiple network views and
overlapping networks in each view then it’s possible that there may be
a network 10.0.0.0/16 defined in more than one view. In that case the
above query would return results like the following:

    <list>
      <value type="object">
        <network>10.0.0.0/16</network>
        <network_view>default</network_view>
        <_ref>network/ZG5zLm5ldHdvcmskMTAuMC4wLjAvMTYvMA:10.0.0.0/16/default</_ref>
      </value>
      <value type="object">
        <network>10.0.0.0/16</network>
        <network_view>test</network_view>
        <_ref>network/ZG5zLm5ldHdvcmskMTAuMC4wLjAvMTYvMw:10.0.0.0/16/test</_ref>
      </value>
    </list>

Just as a program searching for objects has to deal with the
possibility that no such objects are found, it also has to deal with
the possibility that a single unique object is not returned either.

### More on searching for objects

In the previous section we looked at searching for objects by their
names, and noted that a search may fail, in the sense that it may
return nothing at all, or may return more objects than expected. One
way to narrow the search down is to search on multiple fields at
once. For example, although a network address like 10.0.0.0/16 by
itself may not specify a unique network object, the combination of
network address and network view is guaranteed to be unique (assuming
such a network exists).

We can use the following URL to search for the network 10.0.0.0/16 and
specify that we are looking for it in the default network view:

    https://gm.example.com/wapi/v1.0/network?network=10.0.0.0/16&network_view=default

(This assumes that the default network view is actually named
‘default’; it is possible to change the name of the default network
view to something else, and that may be the case in some Infoblox
deployments.)

Similarly, the following URL can be used to search for the overlapping
network 10.0.0.0/16 in the network view ‘test’:

    https://gm.example.com/wapi/v1.0/network?network=10.0.0.0/16&network_view=test

You can also search for objects using other fields in the object, or
using extensible attributes. For example, the following URL would
return all network objects for which the extensible attribute
‘Country’ is set to the value ‘US’:

    https://gm.example.com/wapi/v1.0/network?*Country=US

The syntax is similar to that using fields to search, except that the
extensible attribute name must be prefixed with an asterisk character
(‘\*’).

For a complete list of the fields that can be used to search for data
objects of a given type, see the table in the section “Fields List”
for the object type in question, in the chapter “Objects” in the
official _WAPI Documentation_ reference manual for the version of the
Web API you are using. For a given field the “Search” column of the
“Fields List” table will contain one or more of the following:

* ‘N/A’ indicates that the field cannot be used to search for objects
  of that type.
* ‘=’ indicates that the field can be used to search for objects of
  that type by providing an exact value of the field.
* ‘:’ indicates that the field can be used to do a case-insensitive
  search for objects of that type. Case is ignored when doing the
  search, so (for example) using any of the search strings ‘ab’, ‘AB’,
  ‘Ab’, or ‘aB’ would return the same set of objects.
* ‘~’ indicates that the field can be used to do a search using a
  regular expression.

As an example of a case-insensitive search consider doing a query for
an alternative network view used for testing. You could do the query
operation using the URL

    https://gm.example.com/wapi/v1.0/networkview?name=test

but this would fail if the name of the network view were actually
capitalized as ‘Test’ instead of ‘test’. You could instead use the URL

    https://gm.example.com/wapi/v1.0/networkview?name:=test

with the operator ‘=’ replaced by ‘:=’. A query operation using this
URL would succeed no matter how the name were capitalized, with the
actual value of the name returned in the `name` field for the network
view object.

See below for more information on regular expression searches.

### Searching for networks in a network container

Recall that in the Infoblox NIOS database there is a distinction
between regular networks (which can contain DHCP ranges, fixed
addresses, etc.) and network containers, which contain networks (or
other network containers) but cannot themselves contain DHCP ranges,
etc. Using URLs like those above returns only networks, not network
containers.

Thus, for example, if 10.0.0.0/16 were a network container and not a
network then it would not be returned by the above query. To query the
list of network containers you can use the following URL:

    https://gm.example.com/wapi/v1.0/networkcontainer

Your browser will produce output like the following, assuming that you
have at least one network container defined:

    <list>
      <value type="object">
        <network>10.0.0.0/8</network>
        <network_view>default</network_view>
        <_ref>networkcontainer/ZG5zLm5ldHdvcmtfY29udGFpbmVyJDEwLjAuMC4wLzgvMA:10.0.0.0/8/default</_ref>
      </value>
      <value type="object"><network>10.1.0.0/16</network>
        <network_view>default</network_view>
        <_ref>networkcontainer/ZG5zLm5ldHdvcmtfY29udGFpbmVyJDEwLjEuMC4wLzE2LzA:10.1.0.0/16/default</_ref>
      </value>
      ...
    </list>

At first glance this output looks very similar to the output for
network objects; the major difference is the object type in the object
reference. Network container objects also have a network field, and
that field can be used to search for a particular network container
object just as we used it above to search for a particular network.

From the output in the examples above it appears that the 10.0.0.0/16
network is contained within the 10.0.0.0/8 network container. Suppose
we want to verify this and also to find all other networks within the
10.0.0.0/8 container. You can do this via the Web API by querying for
the following URL:

    https://gm.example.com/wapi/v1.0/network?network_container=10.0.0.0/8

This produces the following output in our example:

    <list>
      <value type="object">
        <comment>QA/test</comment>
        <network>10.0.0.0/16</network>
        <network_view>default</network_view>
        <_ref>network/ZG5zLm5ldHdvcmskMTAuMC4wLjAvMTYvMA:10.0.0.0/16/default</_ref>
      </value>
      <value type="object">
        <network>10.2.0.0/16</network>
        <network_view>default</network_view>
        <_ref>network/ZG5zLm5ldHdvcmskMTAuMi4wLjAvMTYvMA:10.2.0.0/16/default</_ref>
      </value>
      ...
    </list>

Here `network_container` as referenced in the URL is a field
associated with each network object; the value of the field is the
network container within which the network is immediately
contained. (If a given network is not within a network container then
the value of the `network_container` field will be ‘/’.) Although the
value of the `network_container` field is used in the search, that
value is not returned with the network objects by default because it
is not one of the base fields of the network object type (those being
`network`, `network_view`, and `comment`).

### Returning selected fields

If we want to return the network container value for each network
object (for example, to verify that the query results are correct) we
can extend the query URL with an additional option:

    .../v1.0/network?network_container=10.0.0.0/8&_return_fields=network_container

(In this and other examples the beginning of the URL is omitted to
save space.)

The underscore character (‘_’) at the beginning of ‘_return_fields’
identifies it not as a field name but rather as an option for the
query. This query produces output like the following:

    <list>
      <value type="object">
        <network_container>10.0.0.0/8</network_container>
        <_ref>network/ZG5zLm5ldHdvcmskMTAuMC4wLjAvMTYvMA:10.0.0.0/16/default</_ref>
      </value>
      <value type="object">
        <network_container>10.0.0.0/8</network_container>
        <_ref>network/ZG5zLm5ldHdvcmskMTAuMi4wLjAvMTYvMA:10.2.0.0/16/default</_ref>
      </value>
      ...
    </list>

What happened to the `network`, `network_view`, and `comment` fields?
Since we did not specify them as part of the `_return_fields` option
their values were not returned. If we wanted to at least have the
network value returned then we can specify an additional field name
for the `_return_fields` option:

    .../v1.0/network?network_container=10.0.0.0/8&_return_fields=network_container,network

As an alternative we can use the `_return_fields+` option, which
returns the specified field or fields along with all of the base
fields for the object:

    .../v1.0/network?network_container=10.0.0.0/8&_return_fields%2B=network_container

Since ‘+’ is not a permitted character in URLs it has to be encoded as
‘%2B’, i.e., the ‘%’ character followed by the hexadecimal value for
the ‘+’ character. When using the browser we do this manually, but in
practice such URL encoding can be and typically is done by the Python
or other language libraries we use to generate HTTP requests.[^1]

[^1]: The official _WAPI Documentation_ reference manual states that
the following characters need to be %XX encoded in URLs and submitted
form contents: percent sign (‘%’), semicolon (‘;’), forward slash
(‘/’), question mark (‘?’), colon (‘:’), at-sign (‘@’), ampersand
(‘&’), equals sign (‘=’), plus sign (‘+’), dollar sign (‘$’), comma
(‘,’), and space (‘␣’). However in practice many of these are
processed properly even if not encoded.

### Using regular expressions in searches

The examples above showed how to return the networks contained within
a given network container. What if the networks we want are not within
network containers? An alternative is to search for networks or other
data objects using a regular expression, i.e., a search string that
specifies not just a single object but a number of objects that are
similarly named.

For example, suppose we wish to retrieve all private IPv4 networks of
the form ‘192.168.X.Y’ where X and Y are arbitrary octets. This can be
done using the following URL:

    https://gm.example.com/wapi/v1.0/network?network~=192.168.*.*

Using the ‘~=’ operator in the URL rather than the ‘=’ operator tells
the Web API that we are not doing an exact search, but rather are
specifying a regular expression for the search string. The regular
expression in this example is ‘192.168.\*.\*’. The first ‘\*’
character indicates that any octet value is acceptable in the third
octet, and the second ‘\*’ indicates that any value is acceptable in
the fourth octet.

This URL will return results like the following:

    <list>
      <value type="object">
        <comment>Home network</comment>
        <network>192.168.201.0/24</network>
        <network_view>default</network_view>
        <_ref>network/ZG5zLm5ldHdvcmskMTkyLjE2OC4yMDEuMC8yNC8w:192.168.201.0/24/default</_ref>
      </value>
      <value type="object">
        <comment>Guest network</comment>
        <network>192.168.202.0/24</network>
        <network_view>default</network_view>
        <_ref>network/ZG5zLm5ldHdvcmskMTkyLjE2OC4yMDIuMC8yNC8w:192.168.202.0/24/default</_ref>
      </value>
      ...
    </list>

In this case the returned networks may be of any size. If we wanted to
return only /24 networks (for example) we would use the URL

    https://gm.example.com/wapi/v1.0/network?network~=192.168.*.*/24

## Testing object creation, modification, and deletion using Curl

Browsers normally generate HTTP GET requests when accessing web
sites. In the context of the Infoblox NIOS Web API GET requests are
used (only) for queries, while changing data in the grid database is
done using other HTTP requests (POST, PUT, and DELETE). There are ways
to generate these other requests via the browser, but they are not
straightforward.

A better approach to testing object creation and modification via the
Web API is to use a command-line HTTP utility like Curl. Curl can be
used in scripts by itself to do simple object manipulation; however in
this document we concentrate on the use of Curl to test Web API
operations prior to implementing them in a language like Python.

Curl is installed by default on Mac OS X and most versions of
Unix/Linux. For Microsoft Windows binary versions of Curl can be
downloaded from `http://curl.haxx.se` (for installation instructions
do an Internet search for “how to install Curl for Windows”).

### A simple test of the Web API using Curl

Before going on to more complicated operations we first show a basic
test of the Web API using Curl, using the same URL used in the
original test using a browser.

First, access the command line environment on your own system: the
Command Prompt window on Microsoft Windows, the Terminal app on Mac OS
X, or a shell window on Unix/Linux. To verify that you have the Curl
utility installed and accessible, enter the command `curl --version`
by itself. You should get a message like the following (which is for
Mac OS X 10.9):

    curl 7.30.0 (x86_64-apple-darwin13.0) libcurl/7.30.0 SecureTransport zlib/1.2.5
    Protocols: dict file ftp ftps gopher http https imap imaps ldap ldaps pop3 pop3s
      rtsp smtp smtps telnet tftp
    Features: AsynchDNS GSS-Negotiate IPv6 Largefile NTLM NTLM_WB SSL libz

To test the Web API you can then enter the following command in the
command line interface, replacing `gm.example.com` with the
fully-qualified domain name of your grid master (or standalone
appliance, or Cloud Platform grid member) and `admin` with the userid
you use to authenticate:

    curl --tlsv1 --insecure --user 'admin' 'https://gm.example.com/wapi/v1.0/networkview'

Putting quotes around the userid and the URL ensures that any unusual
characters in either value are not inadvertently processed by the
command line shell. For Mac OS X and Unix/Linux you can and should use
single quotes as illustrated above. For Microsoft Windows you must use
double quotes instead:

    curl --tlsv1 --insecure --user "admin" "https://gm.example.com/wapi/v1.0/networkview"

Since the example command lines above do not include a password you
will be prompted to provide it, again as you would be if you tried to
login to the web UI. As with using the Web API via the browser, login
is done using HTTP basic authentication, with the login credentials
protected by SSL/TLS encryption of the browser/grid master
communications.

If you wish you can include the password on the command line as well,
as in the following command:

    curl --tlsv1 --insecure --user 'admin:infoblox' 'https://gm.example.com/wapi/v1.0/networkview'

However you should be aware that doing this can expose the password to
other users who might be logged into the same system.

The `--tlsv1` option tells Curl to use the newer TLS encryption
protocol rather than the older (and less secure) SSL protocol. (The
use of TLS is required for current versions of NIOS and recommended
for older versions as well.) The `--insecure` option tells Curl to
ignore any errors relating to the TLS certificate returned by the
Infoblox appliance. You should try omitting the option on the command
line, for example

    curl --tlsv1 --user 'admin' 'https://gm.example.com/wapi/v1.0/networkview'

and see if an error results. If not then the `--insecure` option is
not needed, and to improve security you should omit it.

All of these options can be abbreviated if desired: The `--tlsv1`
option can be abbreviated as `-1` and the `--insecure` option as `-k`;
the two options can be combined as `-k1`. The `--user` option can also
be abbreviated as `-u`. (For abbreviations of other options execute
the command `curl --help`.)

The structure of the URL supplied to the Curl utility is identical to
that used when accessing the Web API via the browser. However the
output produced by default will look like the following (again,
assuming that you have only a single network view):

    [
      {
        "_ref": "networkview/ZG5zLm5ldHdvcmtfdmlldyQw:default/true",
        "is_default": true,
        "name": "default"
      }
    ]

This output is in JavaScript Object Notation or JSON format, an
alternative format created for use in web-based APIs and related
contexts. Besides being more concise and somewhat simpler to read,
JSON format also has the advantage that it is very similar to the data
structure syntax used in Python.

JSON-formatted output from the Web API consists of a list of data
objects enclosed in square brackets (‘[’ and ‘]’) and separated by
commas (‘,’). In the example above only a single object was
returned. However we can adapt the example above to return a list of
all defined networks using the command

    curl --tlsv1 --insecure --user 'admin' 'https://gm.example.com/wapi/v1.0/network'

This will produce output like the following:

    [
      {
        "_ref": "network/ZG5zLm5ldHdvcmskMTUxLjIwNy4wLjAvMTYvMA:151.207.0.0/16/default",
        "network": "151.207.0.0/16",
        "network_view": "default"
      },
      {
        "_ref": "network/ZG5zLm5ldHdvcmskMTkyLjE2OC4yMDEuMC8yNC8w:192.168.201.0/24/default",
        "comment": "Home network",
        "network": "192.168.201.0/24",
        "network_view": "default"
      },
      ...
    ]

Each data object in the JSON output is represented as a collection of
key/value pairs, enclosed in curly braces (‘{’ and ‘}’) and separated
by commas (‘,’). Each key/value pair contains a field name string in
quotes, followed by a colon (‘:’), followed by the field value. In the
output above the field values are strings (e.g., the network name or
address); however field values can also be of other types as
well. (For example, in the first Curl example the output returned a
network view object with the field `is_default` set to the Boolean
value `true`.)

In most cases it is more convenient to return JSON output from the
Curl command. However if for some reason you need XML output instead
then you can tell the Web API to return XML by using the `--header`
option (abbreviated `-H`) to add a special HTTP header to the request,
as in the following example:

    curl --tlsv1 --insecure --user 'admin' --header 'Accept: application/xml' \
      'https://gm.example.com/wapi/v1.0/network'

(Here we’ve split the command across two lines for readability. On Mac
OS X and Unix/Linux systems you can include the ‘\\’ character at the
end of a line to tell the command line shell that the command will be
continued onto another line.)

This command will produce similar output to that produced in the
browser examples above, except that the output will be in a single
string with no newlines and no indentation.

### Adding a new network using Curl

All of the examples thus far have shown querying for data objects
using the Infoblox NIOS Web API, either using a web browser or using
the Curl utility. All of the queries using a browser can be duplicated
using Curl by using the appropriate URL in place of the ones shown in
the Curl examples above.

However Curl can also be used to add, modify, or delete data objects
with the Web API. This requires specifying a different HTTP request
for Curl to perform, and if needed supplying additional data to be
transmitted as part of the request.

For example, suppose that we wish to create a new network data object
for the network 192.168.100.0/24. We can do this using the following
Curl command (again, the command is split across multiple lines for
readability):

    curl --tlsv1 --insecure \
      --user 'admin' --header 'Content-Type: application/json' \
      --data '{"network": "192.168.100.0/24"}' \
      'https://gm.example.com/wapi/v1.0/network'

(On Microsoft Windows you must use double quotes instead of single
quotes as previously discussed, and must also escape the double quotes
within the string for the `--data` option`:

    curl --tlsv1 --insecure \
      --user "admin" --header "Content-Type: application/json" \
      --data "{\"network\": \"192.168.100.0/24\"}" \
      "https://gm.example.com/wapi/v1.0/network"

For readability this document uses the Unix/Linux/Mac OS X quoting
conventions.)

The `--data` option (abbreviated as `-d`) tells Curl to send an HTTP
POST request with data as specified, and the `--header` (or `-H`)
option tells Curl to send an HTTP header with the request, in this
case to tell the Infoblox system that the data is expressed in JSON
format. Just as a Web API operation to retrieve a data object returns
a collection of key/value pairs representing the object fields and
their values, when creating a data object we specify a collection of
key/value pairs to set the initial field values.

In this case we need only specify the value of the `network` field;
the values of all other fields will default to appropriate values. In
particular the value of the `network_view` field will be set to the
default network view and the value of the `comment` field will be set
to the empty string.

If the Web API operation to add the network is successful then the
operation will return the object reference for the newly-created
network object, for example

    "network/ZG5zLm5ldHdvcmskMTkyLjE2OC4xMDAuMC8yNC8w:192.168.100.0/24/default"

This object reference can be used in further operations as discussed
below.

Suppose that having created the network 192.168.100.0/24 you repeat
the above Curl command and try to create the network again. You will
get an error similar to the following:

    {
      "Error": "AdmConDataError: None (IBDataConflictError: IB.Data.Conflict:The network 192.168.100.0/24 already exists.  Select another network.)",
      "code": "Client.Ibap.Data.Conflict",
      "text": "The network 192.168.100.0/24 already exists.  Select another network."
    }

Note that the error data is itself returned in JSON format.

### Modifying a network object using Curl

Suppose that instead of creating a brand-new data object we wish to
modify an existing object. In the Infoblox NIOS Web API modifications
are done using HTTP PUT requests, with data submitted with the request
to specify the fields to be modified and their new values. The URL for
such a PUT request must include an object reference to the existing
object to be modified. (Note that among other things this means that
you cannot use the Web API to modify multiple objects in a single
operation.)

For example, in the previous example we created a new network
192.168.100.0/24 in the default network view. The Web API operation by
which we created the object returned the (unique) object reference for
the newly-created object:

    network/ZG5zLm5ldHdvcmskMTkyLjE2OC4xMDAuMC8yNC8w:192.168.100.0/24/default

That object reference could also be retrieved by doing a query
operation to search for the network in question, for example using the
Curl command

    curl --tlsv1 --insecure --user admin \
      'https://gm.example.com/wapi/v1.0/network?network=192.168.100.0/24&network_view=default'

which will return output like the following, including the object
reference in the `_ref` field:

    [
      {
        "_ref": "network/ZG5zLm5ldHdvcmskMTkyLjE2OC4xMDAuMC8yNC8w:192.168.100.0/24/default",
        "network": "192.168.100.0/24",
        "network_view": "default"
      }
    ]

Now that we have the object reference for the network of interest,
suppose that we want to modify the object to specify a comment for the
network. We can do this using a Curl command like the following:

    curl --tlsv1 --insecure \
      --user admin --header 'Content-Type: application/json' \
      --data '{"comment": "Lab network"}' --request PUT
      'https://gm.example.com/wapi/v1.0/network/ZG5zLm5ldHdvcmskMTkyLjE2OC4xMDAuMC8yNC8w:192.168.100.0/24/default'

Note that the URL for this command includes the object reference for
the network, and we use the `--request` option to tell Curl to send an
HTTP PUT request instead of an HTTP POST request. (We did not need to
include the `--request` option when creating the object since Curl
does a POST request by default when the `--data` option is specified.)
The fields to be modified are (as in object creation) specified with
the `--data` option using JSON-format key/value pairs.

To confirm that the modification succeeded we can query the object
by its object reference using a Curl command like the following:

    curl --tlsv1 --insecure --user 'admin' \
      'https://gm.example.com/wapi/v1.0/network/ZG5zLm5ldHdvcmskMTkyLjE2OC4xMDAuMC8yNC8w:192.168.100.0/24/default'

which should produce output like

    {
      "_ref": "network/ZG5zLm5ldHdvcmskMTkyLjE2OC4xMDAuMC8yNC8w:192.168.100.0/24/default",
      "comment": "Lab network",
      "network": "192.168.100.0/24",
      "network_view": "default"
    }

In some cases a field value is in the form of a list, for example the
`ipv4addrs` field of a `record:host` object (since a host record may
reference multiple IPv4 addresses). In this case adding a new value to
the list requires first reading the entire value of the field,
including all list items, creating a new list with that value added,
and then doing a PUT request to update the object with the new list as
the field value.

### Modifying extensible attributes using Curl

A similar strategy must be used when updating extensible attributes,
whose names and values are returned as a collection of key/value pairs
when querying for the field name `extensible_attributes` (for Web API
versions 1.0 and 1.1) or `extattrs` (for Web API versions 1.2 or
later).

For example, suppose that the network 192.168.202.0/24 has an
extensible attribute `Country` giving the country in which the network
is located, and we wish to add an additional extensible attribute
`State` specifying the state or province within the country. (The
extensible attribute whose value is being added must already be
defined in the Infoblox system. In this case the extensible attribute
`State`, like `Country`, `Region`, and others, is predefined by NIOS.)

For Web API versions 1.0 and 1.1 the command below will return the
values of all extensible attributes for network 192.168.202.0/24. (For
simplicity we assume that there is only a single network view, so the
query will return at most one network object.)

    curl --tlsv1 --insecure --user 'admin' \
      'https://gm.example.com/wapi/v1.0/network/network=192.168.202.0/24&_return_fields=extensible_attributes'

This will produce output like the following:

    [
      {
        "_ref": "network/ZG5zLm5ldHdvcmskMTkyLjE2OC4yMDIuMC8yNC8w:192.168.202.0/24/default",
        "extensible_attributes": {
          "Country": "US"
        }
      }
    ]

To add a new extensible attribute to the object we must supply a new
value for the entire `extensible_attributes` field:

    curl --tlsv1 --insecure \
      --user 'admin' --header 'Content-Type: application/json' \
      --data '{"extensible_attributes": {"Country": "US", "State": "Maryland"}}' \
      --request PUT \
      'https://gm.example.com/wapi/v1.0/network/ZG5zLm5ldHdvcmskMTkyLjE2OC4yMDIuMC8yNC8w:192.168.202.0/24/default'

This works similarly in Web API versions 1.2 or later, except that the
field `extensible_attributes` is replaced with a new field `extattrs`:

    curl --tlsv1 --insecure --user 'admin' \
      'https://gm.example.com/wapi/v1.2/network?network=192.168.202.0/24&_return_fields=extattrs'

The new field `extattrs` contains multiple data items relating to each
extensible attribute, of which the actual value of the attribute is
only one.

    [
      {
        "_ref": "network/ZG5zLm5ldHdvcmskMTkyLjE2OC4yMDIuMC8yNC8w:192.168.202.0/24/default",
        "extattrs": {
          "Country": {
            "value": "US"
          }
        }
      }
    ]

This makes adding a new extensible attribute value slightly more
complicated:

    curl --tlsv1 --insecure \
      --user 'admin' --header 'Content-Type: application/json'
      --data '{"extattrs": {"Country": {"value": "US"}, "State": {"value": "Maryland"}}}' \
      --request PUT \
      'https://gm.example.com/wapi/v1.2/network/ZG5zLm5ldHdvcmskMTkyLjE2OC4yMDIuMC8yNC8w:192.168.202.0/24/default'

The above example assumes that extensible attribute values are not
inherited. If EA inheritance is used then the field `extattrs` may
contain an additional parameter for each extensible attribute
referencing the object from which that attribute’s value is
inherited. When a new value of `extattrs` is supplied it can contain
other parameters for each extensible attribute specifying how
inheritance should be done for that attribute. For full details see
the section “Extensible attributes inheritance” in the chapter
“Additional Information and Examples” in the official _WAPI
Documentation_ reference manual for your version of the Web API.

### Deleting a network object using Curl

The previous sections showed querying for, creating, and modifying a
data object using Curl, specifically a network object for the network
192.168.100.0/24. Creating a new object using the Infoblox NIOS Web
API returns a unique object reference for the newly-created
object. That object reference is returned when querying for that
object, and is also used when modifying the object.

You can also delete a data object using Curl and the Web API. As with
modifying a data object we must include the object reference in the
URL and also must explicitly specify the type of HTTP request to make,
in this case a DELETE request:

    curl --tlsv1 --insecure --user 'admin' --request DELETE \
      'https://gm.example.com/wapi/v1.0/network/ZG5zLm5ldHdvcmskMTkyLjE2OC4xMDAuMC8yNC8w:192.168.100.0/24/default'

Unlike when modifying an object we do not need to specify any data
using the `--data` object, and thus can also omit the `--header`
option (which we previously used to specify the data as being in JSON
format). The Web API operation will return the object reference for
the object being deleted, similar to the way the object reference was
returned when creating the object.

If you attempt to delete an object that does not exist, for example by
repeating the above Curl command a second time, the Web API operation
will return a JSON-formatted error like the following:

    {
      "Error": "AdmConDataNotFoundError: Reference network/ZG5zLm5ldHdvcmskMTkyLjE2OC4xMDAuMC8yNC8w:192.168.100.0/24/default not found",
      "code": "Client.Ibap.Data.NotFound",
      "text": "Reference network/ZG5zLm5ldHdvcmskMTkyLjE2OC4xMDAuMC8yNC8w:192.168.100.0/24/default not found"
    }

## Querying and manipulating data objects using Python

Using a web browser or the Curl utility is a good way to explore the
Infoblox NIOS Web API and to test Web API operations one at a
time. However production scripts using the Web API will normally be
written in a full-fledged programming language. In this document we
discuss using the Web API from Python, but similar programs can be
written in other languages, including both dynamic languages like
Perl, JavaScript, Ruby, and R as well as more conventional languages
like C++ or Java.

### A simple test of the Web API using Python

Before going on to more complicated operations we first show a basic
test of the Web API using Python, using the same URL used in the
previous tests using a browser and the Curl utility.

As with Curl, using Python typically requires that you access the
command line environment on your own system. Unlike Curl, using the
Web API from Python typically involves first creating a Python program
using a text editor, and then using the `python` command to run the
program. (The `python` command can also be used to invoke an
interactive interpreter for Python, in which you can enter Python
commands and have them be executed immediately.)

To verify that you have Python installed and accessible, enter the
command `python --version` by itself in your command line
environment. It should print the installed version of Python, for
example ‘Python 2.7.6’. If the command does not work see the appendix
“Installing and using Python” for tips on troubleshooting.

Next, create a file named `wapitest.py` using a text editor. (For
example, you can use Notepad on Microsoft Windows, TextEdit on Mac OS
X, and vi or Emacs on Unix/Linux.) Put the following lines into the
file, replacing `gm.example.com` with the fully-qualified domain name
of your grid master (or standalone appliance, or Cloud Platform grid
member) and `admin` and `infoblox` with the userid and password you
use to authenticate:

    import requests
    requests.packages.urllib3.disable_warnings()
    r = requests.get('https://gm.example.com/wapi/v1.0/networkview',
                     auth=('admin', 'infoblox'), verify=False)
    print r.text

The first line allows using functions from the Python Requests module
(`http://python-requests.org`). The Requests module is not included in
the base Python installation but you should install it as an add-on,
as it greatly simplifies doing Web API operations. (See the appendix
“Installing and using Python” for more information.)

The second line suppresses warnings from the lower-level urllib3
module that the Requests module uses for transmitting requests and
receiving responses. This line eliminates warning messages when doing
Web API calls to a grid master that does not have a valid SSL/TLS
certificate; see also below.

The third and fourth lines use the Requests function `requests.get()`
to initiate a Web API query using an HTTP GET request and return a
response, which we store in the variable `r`. (This function call
could be done as a single line in Python, but for readability we split
it into two lines.)

The first argument to the `requests.get()` function is the URL for the
Web API operation. It has the exact same value that we previously used
for testing the Web API from a browser or using Curl.

The second argument to `requests.get()` is a *keyword argument* that
specifies the authentication credentials (i.e., userid and password)
to use when connecting to the grid master. The construct `('admin',
'infoblox')` is a *tuple*, a list of one or more constant values
enclosed in parentheses and separated by commas. Unlike Curl, Python
will not automatically prompt for passwords, so we must either include
the password value in the program or write code to obtain the password
by some other means.

The third argument to `requests.get()` is another keyword argument
that specifies whether or not to try to verify the SSL/TLS certificate
used to encrypt communications between the Python program and the
Infoblox NIOS system referenced in the HTTPS URL. If the Infoblox
system uses a self-signed certificate (which is the default when
setting up a new appliance) then you need to include the `verify`
argument with the value `False`. (This is equivalent to using the
`--insecure` option with Curl.)

You should also try writing the program without including the `verify`
argument and without suppressing warnings from urllib3:

    import requests
    r = requests.get('https://gm.example.com/wapi/v1.0/networkview',
                     auth=('admin', 'infoblox'))
    print r.text

If this version of the program works the same way as the original
version then for enhanced security you can and should omit using
`verify=False` and should not suppress urllib3 warnings. (To save
space we will omit the `verify=False` and urllib3 warning suppression
in subsequent example code.)

The fifth and final line of the program prints the results of the Web
API operation. The variable `r` is a _Response object_ that has
various attributes. In particular `r.text` contains the HTTP response
from the Infoblox system, formatted as a text string.

To test the Python program you can then enter the following command in
the command line interface,

    python wapitest.py

The output should look similar to the JSON-formatted output produced
by default by Curl, for example the following (assuming that you have
only a single network view):

    [
      {
        "_ref": "networkview/ZG5zLm5ldHdvcmtfdmlldyQw:default/true",
        "is_default": true,
        "name": "default"
      },
    ]

### Processing Web API results in Python

Typically rather than just printing out the result of a Web API
operation we want to process the returned data in some way. To do that
in our example program we reference `r.json()`, where `json()` is a
*method*, a special type of function that is associated with the
Response object `r`. The `json()` method takes the JSON-formatted
result of the Web API operation and converts it into native Python
data types.

For example, if we modify the fourth line of the program above to
produce the following new program:

    import requests
    r = requests.get('https://gm.example.com/wapi/v1.0/networkview',
                     auth=('admin', 'infoblox'))
    print r.json()

it will produce output like the following:

    [{u'is_default': True, u'_ref': u'networkview/ZG5zLm5ldHdvcmtfdmlldyQw:default/true',
    u'name': u'default'}]

Except for the formatting this is almost identical to the output from
previous programs. The main difference is that the strings are
prefixed by the letter ‘u’ to mark that they are *Unicode strings*
capable of containing any valid character, including characters from
languages other than English that are not representable in the
traditional US-ASCII set.

Suppose we just want to see the name of the network view and not the
object reference or whether it is the default view. The following
modified version of `wapitest.py` does that:

    import requests
    r = requests.get('https://gm.example.com/wapi/v1.0/networkview',
                     auth=('admin', 'infoblox'))
    nview = r.json()[0]
    print nview['name']

Here for convenience we assign the data returned by `r.json()` to a
variable `nview`. Since `r.json()` returns a list we use the index
`[0]` to tell Python that we want only the first element in that
list. (In Python lists are indexed from 0.) That element (which we
assign to the variable `nview`) is a Python *dictionary* containing a
collection of key/value pairs representing the object’s fields and
their values. To return the value for the name of the network view we
index `nview` with `['name']` to return the value of the `name` field.

This program will produce output like the following:

    default

Suppose that your Infoblox system actually has more than one network
view defined. In that case printing the value of `r.json()` will
result in output like

    [{u'is_default': True, u'_ref': u'networkview/ZG5zLm5ldHdvcmtfdmlldyQw:default/true',
    u'name': u'default'}, {u'is_default': False,
    u'_ref': u'networkview/ZG5zLm5ldHdvcmtfdmlldyQz:test/false', u'name': u'test'}]

and the program above will output only the name of the first network
view in the list. (That network view may be the default view, as in
the example above, but in general the Web API does not guarantee that
results will be returned in any particular order.)

The following modified program will print out the names of all network
views, along with the field `is_default` that specifies which network
view is the default network view.

    import requests
    r = requests.get('https://gm.example.com/wapi/v1.0/networkview',
                     auth=('admin', 'infoblox'))
    nviews = r.json()
    for nview in nviews:
        print nview['name'], nview['is_default']

In this program the entire list returned by `r.json()` is assigned to
the variable `nviews`, and a Python *for loop* is used to take each
element of the list in turn and assign it to the variable `nview`. As
in the previous program the value of `nview` is a Python
dictionary. (In contrast `r.json()` and `nviews` are lists of
dictionaries, one per object returned.) The program then prints the
values of the two fields, again using the field names to look up the
field values within the dictionary.

The `print` statement in this program is indented relative to the
`for` statement (and relative to the `print` statement in the previous
program). This indentation is mandatory: It tells Python that the
`print` statement is part of the code executed for each list element,
and not code that comes after the loop.

The output of the program should look like the following:

    default True
    test False

Here the second network view is named ‘test’, and the default network
view is named ‘default’, as in the previous example. (Recall that the
default network view cannot be deleted but can be renamed. Thus its
name may not always be ‘default’.)

### Searching for objects using Python

Searching for objects using the Web API and Python works very much
like it does using a browser or Curl. For example, here is a program
to search for the network object for the network 10.0.0.0/16 and print
its name, the network view in which the network is defined, and any
comment associated with the network:

    import requests
    import sys
    r = requests.get('https://gm.example.com/wapi/v1.0/network?network=10.0.0.0/16',
                     auth=('admin', 'infoblox'))
    nets = r.json()
    if not nets:
        sys.exit('network not found')
    elif len(nets) > 1:
        sys.exit('network not unique')
    net = nets[0]
    print net['network'], net['network_view'], net.get('comment', '')

If the Web API operation executed successfully then `r.json()`should
be defined and contain a list of any objects returned, which we assign
to the variable `nets`. If no objects were returned then `nets` will
contain an empty list and the condition `not nets` will be true. In
this case we call the function `sys.exit()` to immediately terminate
the program with an error message. (This function is part of the `sys`
module, which we import at the beginning of the program along with the
Requests module.)

Otherwise we check the length of the list contained in `nets` to see
if more than one network object was returned, that is, the network
being searched for was not unique. (`elif` is an abbreviation for
“else if”.) If this is the case then again we call `sys.exit()` to
terminate the program with an error message.

Once we verify that the list of networks returned contains one and
only one network, we assign the first (and only) item in `nets` to the
variable `net`. Since `net` contains a single network object we can
then extract the values of the fields of that object by indexing `net`
using a field name. However we have to be careful because not all
possible fields of the object are guaranteed to be present. In this
case the `network` and `network_view` fields are always defined for a
network object, but the `comment` field is defined only if a comment
was specifically assigned to the network. If no comment was set then
the reference `net['comment']` will return a Python ‘KeyError’ and
cause the program to terminate.

The problem of missing fields can be resolved in more than one
way. One approach is to explicitly check to see if the field is
defined, as in the following example code snippet that sets the
variable `cmt` to the empty string if there is no key/value pair in
`net` with a key of `'comment'`:

    if 'comment' not in net:
        cmt = ''
    else:
        cmt = net['comment']

Another, and usually simpler, approach is to use the `get()` method on
the object `net`:

    cmt = net.get('comment', '')

The `get()` method allows specifying a default value (in this case the
empty string) to be returned if the field is not present in the
object.

In the example program above we checked to verify that only one
network was returned by the search. We could have avoided the need for
this check by specifying the network view along with the network when
doing the search, using a URL like

    https://gm.example.com/wapi/v1.0/network?network=10.0.0.0/16&network_view=default

We could have also added the `_return_fields` option if we wanted to
retrieve a specific set of fields for the network object, as discussed
in a previous section.

### Passing option arguments using Python

Adding lots of options will make the URL passed to `requests.get()`
very long and make the code harder to read. One way to make the code
more clear is simply to use Python string concatenation (using the ‘+’
operator) to reduce the length of the code lines, as in the following
code snippet:

    r = requests.get('https://gm.example.com/wapi/v1.0/network?' +
                         'network=10.0.0.0/16' + '&' +
                         'network_view=default' + '&' +
                         '_return_fields=network,comment',
                     auth=('admin', 'infoblox'))

Another way is to use the `params` keyword argument to
`requests.get()` to pass a dictionary containing the options as
key/value pairs:

    opts = {'network': '10.0.0.0/16',
            'network_view': 'default',
            '_return_fields': 'network,comment'}
    r = requests.get('https://gm.example.com/wapi/v1.0/network',
                     params=opts,
                     auth=('admin', 'infoblox'))

This second approach has the advantage that `requests.get()` will
automatically encode any problematic characters in field names and
search values. For example, when using the `_return_fields+` option
you can write code like

    opts = {'network': '10.0.0.0/16',
            'network_view': 'default',
            '_return_fields+': 'members'}

instead of having to encode the ‘+’ character in `_return_fields+` as
‘%2B’ (as shown in a previous section).

(If you want to verify that the options are encoded correctly you can
print the variable `r.url` after a successful call to
`requests.get()`. It will show the complete URL as sent to the
Infoblox system.)

However the `params` keyword argument can be used only in cases where
the desired options are of the form `x=y`. It cannot be used when
doing case-insensitive searches of the form `x:=y` or regular
expression searches of the form `x~=y`.

In either of the above approaches you can use variables to specify the
objects being searched for, as opposed to using string literals. For
example, in the following code snippets the network to search for is
stored in the variable `net_to_search`:

    net_to_search = '10.0.0.0/16'
    r = requests.get('https://gm.example.com/wapi/v1.0/network?' +
                         'network=' + net_to_search + '&' +
                         'network_view=default' + '&' +
                         '_return_fields=network,comment',
                     auth=('admin', 'infoblox'))

or

    net_to_search = '10.0.0.0/16'
    opts = {'network': net_to_search,
            'network_view': 'default',
            '_return_fields': 'network,comment'}
    r = requests.get('https://gm.example.com/wapi/v1.0/network',
                     params=opts,
                     auth=('admin', 'infoblox'))

### Adding, modifying, and deleting objects using Python

Python can be used to create objects using the `requests.post()`
function. For example, suppose that we wish to create a new network
data object for the network 192.168.100.0/24. We can do this using the
following program:

    import requests
    import sys
    import json
    data_to_send = {'network': '192.168.100.0/24',
                    'network_view': 'default'}
    r = requests.post('https://gm.example.com/wapi/v1.0/network',
                      data=json.dumps(data_to_send),
                      auth=('admin', 'infoblox'))
    net_ref = r.json()
    if not net_ref:
        sys.exit('network object not returned')
    print net_ref

As we did when sending a POST request with Curl, we must supply the
data to be used when creating the object, using the `data` keyword
argument. We define the data as a Python dictionary containing the
fields for the object and their initial values. Each type of object
has a set of fields whose values need to be specified when creating an
object of that type. (See the official _Infoblox WAPI Documentation_
reference manual for the version of the Web API you are using.) When
creating a new network object we need specify only the `network`
field; however for clarity we specify the network view as well.

Having defined the dictionary of key/value pairs for the object’s
fields, we then use the `json.dumps()` function to convert the
dictionary to a JSON-formatted string prior to passing it to
`requests.post()`. (We import the json module in order to use this
function and any other related ones we might need.)

Instead of returning a list of objects like `requests.get()`, the
`requests.post()` function returns a single JSON-formatted character
string that should contain the object reference for the
newly-created object, like the following:

    network/ZG5zLm5ldHdvcmskMTkyLjE2OC4xMDAuMC8yNC8w:192.168.100.0/24/default

Once you have an object reference, whether from a call to
`requests.post()` or through searching for the object using
`requests.get()`, you can use the `requests.put()` function to modify
the fields of the object. For example, having created the network
192.168.100.0/24 you can subsequently retrieve its object reference
and modify the network object to add a comment:

    import requests
    import sys
    import json

    opts = {'network': '192.168.100.0/24',
            'network_view': 'default'}
    r = requests.get('https://gm.example.com/wapi/v1.0/network',
                     params=opts,
                     auth=('admin', 'infoblox'))
    nets = r.json()
    if not nets:
        sys.exit('network not found')
    elif len(nets) > 1:
        sys.exit('network not unique')
    net_ref = nets[0]['_ref']

    data_to_send = {'comment': 'Test network'}
    r = requests.put('https://gm.example.com/wapi/v1.0/' + net_ref,
                      data=json.dumps(data_to_send),
                      auth=('admin', 'infoblox'))

The first section of the program uses `requests.get()` to search for
the desired network object using its network address and network
view. Assuming the object is found we then get the object reference
from the `_ref` field of the first (and only) object returned.

The second section of the program uses `requests.put()` to modify the
object, passing a URL containing the complete object reference. Unlike
the earlier example using Curl, here we have the object reference
stored in a variable and use Python string concatenation to append it
to the fixed part of the URL. As with the example using
`requests.post()` to create a network object, we put the field name
and value in a Python dictionary as a key/value pair, and pass a
JSON-formatted string containing that dictionary to `requests.post()`
using the `data` keyword argument.

Finally, the `requests.delete()` function can be used to delete an
object given its object reference, as in the following program:

    import requests
    import sys
    import json

    opts = {'network': '192.168.100.0/24',
            'network_view': 'default'}
    r = requests.get('https://gm.example.com/wapi/v1.0/network',
                     params=opts,
                     auth=('admin', 'infoblox'))
    nets = r.json()
    if not nets:
        sys.exit('network not found')
    elif len(nets) > 1:
        sys.exit('network not unique')
    net_ref = nets[0]['_ref']

    r = requests.delete('https://gm.example.com/wapi/v1.0/' + net_ref,
                        auth=('admin', 'infoblox'))

The first part of the program (retrieving the object reference for the
network) is identical to the previous program. In the second part we
call `requests.delete()` instead of `requests.put()`; we do not need
to pass any data since the object to delete is already referenced in
the URL.

### Handling errors in WAPI operations using Python

When using the Web API using Python or other languages you need to
account for the possibility of errors being returned from Web API
operations. For example, in the following code

    import requests
    r = requests.get('https://gm.example.com/wapi/v1.0/networkview',
                     auth=('admin', 'infoblox'))
    nviews = r.json()

we are assuming that `requests.get()` will complete execution and will
return some sort of valid result to be assigned into the variable
`r`. However in general it may be the case that this does not happen:
the grid master cannot be reached, the system does not support the Web
API version requested, the authentication credentials are incorrect,
the parameters for the API operation are incorrect, or any of a number
of other errors prevent successful completion of the Web API
operation.

Error handling requires determining whether an error has occurred and
then deciding what action to take. There are two general types of
errors that might prevent successful completion of an API request.

In the first type of error the `requests.get()` function completes
execution successfully, but the API operation itself does not produce
the expected result. In this case you can determine whether an error
has occurred by checking the value of `r.status_code` where `r` is the
result from calling `requests.get()` or other Requests functions. The
status code returned can take one of several values based on the HTTP
response code returned by the Web API operation. The most common
status code indicating success is 200, as in the following incomplete
code snippet:

    r = requests.get('https://gm.example.com/wapi/v1.0/networkview',
                     auth=('admin', 'infoblox'))
    if r.status_code != 200:
        print 'An error has occurred!'

To make the code more understandable the Requests package provides
predefined constants corresponding to the various numeric HTTP status
codes. For example, `requests.codes.ok` is the constant having the
value 200, so the following code is equivalent to the example above:

    r = requests.get('https://gm.example.com/wapi/v1.0/networkview',
                     auth=('admin', 'infoblox'))
    if r.status_code != requests.codes.ok:
        print 'An error has occurred!'

The following table has a list of the most common status codes and
their meanings. For example, you would use the constant
`requests.status_code.unauthorized` to check for the case where the
authentication credentials are incorrect.

+-------------+--------------+-------------------------------------+
| Status code | Constant     | Typical meaning                     |
+=============+==============+=====================================+
| 200         | ok           | The operation was successful (used  |
|             |              | with `requests.get()`,              |
|             |              | `requests.put()`,                   |
|             |              | and `requests.delete()`)            |
+-------------+--------------+-------------------------------------+
| 201         | created      | The object was successfully created |
|             |              | (used with `requests.post()`)       |
+-------------+--------------+-------------------------------------+
| 400         | bad_request  | The object type or field name       |
|             |              | referenced was not                  |
|             |              | known or not supported in this      |
|             |              | version of the Web API              |
+-------------+--------------+-------------------------------------+
| 401         | unauthorized | The login id and/or password was    |
|             |              | incorrect or did not have proper    |
|             |              | permissions for the requested       |
|             |              | operation                           |
+-------------+--------------+-------------------------------------+

For the complete list see the source code for the Requests module at
`https://github.com/kennethreitz`.

If `requests.get()` does return an error, what should the program do?
In general the program must do more than just print an error message,
since the error may prevent subsequent code from executing
correctly. For simplicity, in our example programs we will simply stop
the program if an error is encountered.

For example, the following program modifies our original test program to
provide more robust error handling:

    import requests
    import sys
    r = requests.get('https://gm.example.com/wapi/v1.0/networkview',
                     auth=('admin', 'infoblox'))
    if r.status_code != requests.codes.ok:
        sys.exit('Error finding network views')
    print r.text

Here we do the same check on `r.status_code` as in the example above,
but instead of simply printing an error message we call the function
`sys.exit()` to terminate the program. This function is part of the
`sys` module, which we import in the second line.

The `sys.exit()` function takes a character string argument indicating
the type of error causing program termination. In the example above
that string contains a generic message that doesn’t provide any
information about the exact cause of the error. The example below
contains additional code to include the numeric status code
(`r.status_code`) and an associated informational message
(`r.reason`), as well as additional information useful for debugging.

    import requests
    import sys
    r = requests.get('https://gm.example.com/wapi/v1.0/networkview',
                     auth=('admin', 'infoblox'))
    if r.status_code != requests.codes.ok:
        print r.text
        exitmsg = 'Error {} finding network views: {}'
        sys.exit(exitmsg.format(r.status_code, r.reason))
    print r.text

The error message variable `exitmsg` contains two occurrences of
‘{}’. These serve as placeholders indicating where to include the
status code and associated reason. We then call the `format()` method
for the string `exitmsg` and supply the two variables whose values we
wish to include in the final error message. The `format()` method
replaces the two occurrences of ‘{}’ in `exitmsg` and returns a new
character string that is passed to `sys.exit()`.

Unfortunately the status code and reason are not always sufficient to
determine why an error occurred. In particular status code 400 (bad
request) is generated for many different reasons, including attempting
to use an unsupported Web API version, requesting an unsupported
object, misspelling a field name, or using an incorrect format for
supplied field values.

In some of these cases (like requesting an unsupported object) the
response will include JSON-formatted fields supplying more information
about the error; see the section “Using a browser to retrieve objects”
for an example of this. In other cases (like using an unsupported WAPI
version) the response will consist of a simple (non-JSON) text string.
For maximum generality the code above simply prints the text version
of the response (`r.text` in this case) before calling `sys.exit()`.

This concludes discussion of the first type of error possible for a
Web API operation. In the second type of error the HTTP request for
the API operation fails in some way--for example, the FQDN for the
grid master does not resolve or the grid master itself does not
respond. In these types of errors (which typically occur in
lower-level networking code) the relevant Requests function (e.g.,
`requests.get()`) does not complete and return an error code. Instead
execution of the Requests function is interrupted and program control
is transferred elsewhere using the Python _exceptions_ mechanism.

Normally an exception raised in a Requests function will cause an
error message to be printed and the program to be terminated. It is
possible to include code in your program to catch exceptions and deal
with them yourself. However there are many possible exceptions the
program might have to deal with, writing code to handle them properly
is not trivial, and in most cases it is not possible to recover from
them anyway.

This document therefore does not discuss handling Python exceptions
caused by Web API operations. If you are interested in further
information on this topic see the section on exceptions in the
“Developer Interface” page on the Python Requests web site
(`http://docs.python-requests.org`).

### Generating CSV files using Python

One common use of the Infoblox NIOS Web API is to create custom
reports that go beyond what can be produced using either the Trinzic
Reporting appliance or via a standard CSV export operation using the
Infoblox Grid Manager web interface. This section shows how to use the
Web API to retrieve data and produce CSV files as output. You can then
open these CSV files in a spreadsheet application like Microsoft Excel
and add further formatting to produce a report.

The Python csv module can be used to read and write CSV-format
files. The example program below retrieves a list of all network
containers and writes the list as a CSV file.

    import requests
    import sys
    import csv

    r = requests.get('https://gm.example.com/wapi/v1.0/networkcontainers',
                     auth=('admin', 'infoblox'))
    if r.status_code != requests.codes.ok:
        print r.text
        exitmsg = 'Error {} finding network containers: {}'
        sys.exit(exitmsg.format(r.status_code, r.reason))
    containers = r.json()

    with open('network_containers.csv', 'wb') as out_file:
        out_csv = csv.writer(out_file,
                             delimiter=',',
                             quotechar='"',
                             quoting=csv.QUOTE_MINIMAL)
        out_csv.writerow(['Network Container',
                          'Network View',
                          'Comment'])
        for container in containers:
            out_csv.writerow([container['network'],
                              container['network_view'],
                              container.get('comment', '')])

The first section of the program imports the csv package along with
the requests and sys modules. The second section is similar to prior
Python example programs; it simply uses the `requests.get()` function
to retrieve a list of all network containers and then stores that list
in the variable `containers`.

The third section writes the CSV output file. The `with open(...) as
out_file` statement opens the output file `network_containers.csv` for
writing and then saves the resulting file handle in the variable
`out_file`. It then executes the following indented block of code as a
unit; if an error occurs while executing the block then the file will
be closed automatically and other cleanup done.

The code within the `with` block then does the following:

* It uses the `csv_writer()` function to return an object `out_csv`
  that can be used in writing a CSV file. The parameters to
  `csv_writer()` specify the separator between values in a row (in
  this case a comma) and how quoting of values is to be done (in this
  case we use double quotes to enclose values with embedded commas).
* It then uses the `writerow()` method of the `out_csv` object to
  write a header line for the CSV file. The output file will have
  three columns containing the network container itself, the network
  view in which the container is itself contained, and any comment
  associated with the network container.
* Finally the code loops over the list of network containers stored in
  the `containers` variable, and processes each list item in turn
  using the variable `container`. For each network container we write
  a row of the CSV file containing the network container in CIDR
  format (from the `network` field), the network view of the container
  (the `network_view` field), and any comment for the container. For
  the comment we use the `get()` method as discussed above to handle
  the case where the container may not have a comment associated with
  it.

The program above will produce output like the following:

    Network Container,Network View,Comment
    10.0.0.0/8,default,
    10.1.0.0/16,default,
    192.168.201.0/24,test,
    172.16.0.0/16,default,Dev networks
    172.17.0.0/16,default,QA networks

Note that the output file is not sorted in any way. You can sort the
rows using a spreadsheet application or sort the results prior to
writing the CSV file, as described below.

The next example program shows creation of a more complicated report
in CSV format, namely reporting the number of IP addresses used on all
IPv4 networks.

    import requests
    import json
    import csv

    r = requests.get('https://gm.example.com/wapi/v1.0/network',
                     params={'_max_results': str(2000)},
                     auth=('admin', 'infoblox'))
    if r.status_code != requests.codes.ok:
        print r.text
        exitmsg = 'Error {} finding networks: {}'
        sys.exit(exitmsg.format(r.status_code, r.reason))
    networks = r.json()

    stats = []
    for network in networks:
        # Retrieve used ipv4address objects for this network.
        r = requests.get('https://gm.example.com/wapi/v1.0/ipv4address',
                         params={'network': network['network'],
                                 'network_view': network['network_view'],
                                 'status': 'USED',
                                 '_return_fields': '',
                                 '_max_results': str(10000)},
                         auth=('admin', 'infoblox'))
        if r.status_code != requests.codes.ok:
            print r.text
            exitmsg = 'Error {} finding IP addresses: {}'
            sys.exit(exitmsg.format(r.status_code, r.reason))
        ipv4addresses = r.json()

        # Count the number of ipv4address objects and save the count.
        used = len(ipv4addresses)
        prefix = int(network['network'].split('/')[1])
        size = 2**(32-prefix) - 2
        pct_used = round((100. * used) / size, 1)
        stats.append([network['network'],
                      network['network_view'],
                      size,
                      used,
                      pct_used])

    with open('ipam-stats.csv', 'wb') as out_file:
        out_csv = csv.writer(out_file,
                             delimiter=',',
                             quotechar='"',
                             quoting=csv.QUOTE_MINIMAL)
        out_csv.writerow(['Network',
                          'Network View',
                          'Size',
                          'Used',
                          '% Used'])
        for item in sorted(stats):
            out_csv.writerow(item)

Unlike the first example, in which the result of the Web API call was
directly used to create the CSV output file, here a series of Web API
calls is required to gather the number of IP addresses used for each
network and then to calculate the network utilization. The resulting
information is stored in an intermediate variable `stats` that is a
list of lists, with one list of values produced for each network. The
program then iterates over that list of lists (after first sorting it)
and for each list of network values writes a row in the CSV output
file.

(This program touches each network object only once, so a list works
well for recording results. In some cases you might need to later come
back and add additional values to the information recorded for each
network. In those cases a Python dictionary could be used to store
results instead of a list, with the dictionary keys being the network
values in CIDR format. Each dictionary item could in turn be either a
list or another dictionary, depending on what makes most sense.)

The program also shows another feature of the Web API not previously
discussed. The option `_max_results` is used to limit the number of
results to be returned to the program in a single Web API call. This
option can be used when the number of networks or the number of
addresses per network exceeds the default limit of 1000 results
returned.

The program above will produce output like the following:

    Network,Network View,Size,Used,% Used
    10.0.0.0/16,default,65534,0,0.0
    10.0.0.0/16,test,65534,0,0.0
    10.1.0.0/24,default,254,8,3.1
    10.2.0.0/16,default,65534,0,0.0
    172.17.0.0/17,default,32766,0,0.0
    172.17.128.0/17,default,32766,0,0.0
    192.168.101.0/24,test,254,0,0.0
    192.168.201.0/24,default,254,130,51.2
    192.168.201.0/26,test,62,1,1.6
    192.168.202.0/24,default,254,58,22.8

The list is sorted based on all the values of the list items for each
network’s list stored in the `stats` list of lists; since the network
value in CIDR format is the first item in those lists, the resulting
output is sorted by network.

## Sample WAPI programs

*To be written.*

## Appendix: Differences among Web API versions

Web API version numbers are typically of the form *major.minor* where
*major* is the major release number and *minor* is the minor release
number. The Web API major version number is incremented only for major
releases of the Infoblox NIOS software; for example, all NIOS 6.x
releases use 1 as the Web API version number, while NIOS 7.x releases
use 2 as the Web API version number. The Web API minor version number
is incremented for NIOS minor releases; for example, for NIOS 6.6 the
Web API version number was 1.0, while for NIOS 6.7 the Web API version
number was 1.1.

Some NIOS releases have Web API version numbers of the form
*major.minor.patch* where *patch* indicates that a change was made to
the Web API for a patch release of NIOS. For example, NIOS 6.8.2 has
the Web API version number 1.2.1.

The table below gives Web API version numbers, associated NIOS
releases, and a description of major Web API changes for all Web API
versions to date.

+--------------+------------------+---------------------------------+
| WAPI version | NIOS release     | Major Web API changes           |
+==============+==================+=================================+
| 1.0          | 6.6              | Initial Web API implementation  |
+--------------+------------------+---------------------------------+
| 1.1          | 6.7              | Support DNS zones, DHCP leases, |
|              |                  | internationalized domain names  |
+--------------+------------------+---------------------------------+
| 1.2          | 6.8              | Support named ACLs, extensible  |
|              |                  | attribute inheritance           |
+--------------+------------------+---------------------------------+
| 1.2.1        | 6.8.2            | Add function to select next     |
|              |                  | available IP address when       |
|              |                  | creating certain objects        |
+--------------+------------------+---------------------------------+
| 1.3          | 6.9 LD           | Support selection of next       |
|              |                  | available IP address when       |
|              |                  | creating additional objects     |
+--------------+------------------+---------------------------------+
| 1.4          | 6.10             | Support Network Insight         |
+--------------+------------------+---------------------------------+
| 1.4.1        | 6.10.*??*        | *Support NAPTR records?*        |
+--------------+------------------+---------------------------------+
| 1.4.2        | 6.10.*??*        | *Support NAPTR records?*        |
+--------------+------------------+---------------------------------+
| 1.5          | 6.11 LD          | Support DHCP roaming hosts      |
+--------------+------------------+---------------------------------+
| 1.6          | 6.11             | Support shared records,         |
|              |                  | network templates, DNS          |
|              |                  | integrity checks, defining      |
|              |                  | extensible attributes           |
+--------------+------------------+---------------------------------+
| 1.7          | 6.12             | Enhance DHCP properties support |
+--------------+------------------+---------------------------------+
| 2.0          | 7.0              | Support Cloud Network           |
|              |                  | Automation, Cloud Platform      |
|              |                  | appliances, DNS Traffic         |
|              |                  | Control, Active Directory Sites |
+--------------+------------------+---------------------------------+

### Web API version 1.0 (NIOS 6.6)

Web API version 1.0 supported the following initial set of objects:

* ipv4address
* ipv6address
* ipv6network
* ipv6networkcontainer
* ipv6range
* macfilteraddress
* network
* networkcontainer
* networkview
* range
* record:a
* record:aaaa
* record:cname
* record:host
* record:host_ipv4addr (in record:host)
* record:mx
* record:ptr
* record:srv
* record:txt
* search

### Web API version 1.1 (NIOS 6.7)

Web API version 1.1 introduced the following new objects:

* fixedaddress
* grid
* ipv6fixedaddress
* lease
* member
* record:host_ipv6addr (in record:host)
* restartservicestatus
* scheduledtask
* view
* zone_auth
* zone_delegated
* zone_forward
* zone_stub

Web API 1.1 added new fields to existing objects as follows:

* ipv4_addresses
    - fingerprint
* range
    - fingerprint_filter_rules
* record:a
    - dns_name
* record:aaaa
    - dns_name
* record:cname
    - dns_canonical
    - dns_name
* record:host
    - dns_aliases
    - dns_name
    - ipv6addr (search-only)
* record:cname
    - dns_mail_exchanger
    - dns_name
* record:ptr
    - dns_name
    - dns_ptrdname
    - ipv6addr
* record:srv
    - dns_name
    - dns_target
* record:txt
    - dns_name

### Web API 1.2 (NIOS 6.8)

Web API version 1.2 introduced the following new object:

* namedacl

Web API 1.2 added new fields to existing objects as follows:

* record:host_ipv4addr
    - use_for_ea_inheritance
* record:host_ipv6addr
    - use_for_ea_inheritance
* search
    - objtype

As part of the support for extensible attribute inheritance Web API
1.2 also replaced the field `extensible_attributes` with the new field
`extattrs` in the following objects:

* ipv4address
* ipv6address
* ipv6fixedaddress
* ipv6network
* ipv6networkcontainer
* ipv6range
* macfilteraddress
* network
* networkcontainer
* networkview
* range
* record:a
* record:aaaa
* record:cname
* record:host
* record:mx
* record:ptr
* record:srv
* record:txt
* view
* zone_auth
* zone_delegated
* zone_forward
* zone_stub

### Web API 1.2.1 (NIOS 6.8.2)

Web API version 1.2.1 did not introduce any new objects.

Web API 1.2.1 introduced a new function `func:nextavailableip` to
automatically select the next available IP address for the `ipv4addr`
or `ipv6addr` fields when creating the following objects:

* fixedaddress
* ipv6fixedaddress
* record:host_ipv4addr (in record:host)
* record:host_ipv6addr (in record:host)

### Web API 1.3 (NIOS 6.9 LD)

Web API version 1.3 did not introduce any new objects.

Web API 1.3 added support for use of the function
`func:nextavailableip` with the `ipv4addr` or `ipv6addr` fields when
creating the following objects:

* record:a
* record:aaaa
* record:ptr

### Web API 1.4 (NIOS 6.10)

Web API version 1.4 introduced the following new objects:

* allrecords
* discovery:device
* discovery:deviceinterface
* discovery:deviceneighbor
* discovery:status
* fileop
* grid:dhcpproperties
* ipv6sharednetwork
* permission
* sharednetwork
* snmpuser

### Web API 1.4.1 (NIOS *6.10.??*)

*To be written.*

### Web API 1.4.2 (NIOS *6.10.??*)

*To be written.*

### Web API 1.5 (NIOS 6.11 LD)

*This section will need to be revised to reflect WAPI 1.4.1, etc.*

Web API version 1.5 introduced the following new objects:

* csvimporttask
* roaminghost

Web API 1.5 introduced a new function `func:nextavailablenetwork` to
automatically select the next available network with selected CIDR in
a specified network or network container when creating the following
objects:

* ipv6network
* ipv6networkcontainer
* network
* networkcontainer

### Web API 1.6 (NIOS 6.11)

Web API version 1.6 introduced the following new objects:

* discovery
* discovery:devicecomponent
* extensibleattributedef
* ipv6networktemplate
* networktemplate
* sharedrecord:a
* sharedrecord:aaaa
* sharedrecord:mx
* sharedrecord:srv
* sharedrecord:txt
* zone_auth_discrepancy

Web API 1.6 added new fields to existing objects as follows:

* discovery
    - cap_admin_status_ind
    - cap_admin_status_na_reason
    - cap_description_ind
    - cap_description_na_reason
    - cap_net_deprovisioning_ind
    - cap_net_deprovisioning_na_reason
    - cap_net_provisioning_ind
    - cap_net_provisioning_na_reason
    - cap_net_vlan_provisioning_ind
    - cap_net_vlan_provisioning_na_reason
    - cap_vlan_assignment_ind
    - cap_vlan_assignment_na_reason
    - cap_voice_vlan_ind
    - cap_voice_vlan_na_reason
    - extattrs
    - network_infos
    - port_stats
    - vlan_infos
    - discovery_member (search-only)
* discovery:deviceinterface
    - admin_status_task_info
    - cap_if_admin_status_ind
    - cap_if_admin_status_na_reason
    - cap_if_description_ind
    - cap_if_description_na_reason
    - cap_if_net_deprovisioning_ipv4_ind
    - cap_if_net_deprovisioning_ipv4_na_re
    - cap_if_net_deprovisioning_ipv6_ind
    - cap_if_net_deprovisioning_ipv6_na_re
    - cap_if_net_provisioning_ipv4_ind
    - cap_if_net_provisioning_ipv4_na_reas
    - cap_if_net_provisioning_ipv6_ind
    - cap_if_net_provisioning_ipv6_na_reas
    - cap_if_vlan_assignment_ind
    - cap_if_vlan_assignment_na_reason
    - cap_if_voice_vlan_ind
    - cap_if_voice_vlan_na_reason
    - description_task_info
    - extattrs
    - reserved_object
    - vlan_info_task_info
* discovery:status
    - cli_collection_enabled
* fixedaddress
    - allow_telnet
    - cli_credentials
    - device_description
    - device_location
    - device_type
    - device_vendor
    - discover_now_status
    - is_invalid_mac
    - reserved_interface
    - use_cli_credentials
    - discovered_data.iprg_no (search-only)
    - discovered_data.iprg_state (search-only)
    - discovered_data.iprg_type (search-only)
* ipv4address (in record:host)
    - conflict_types
    - discover_now_status
    - is_invalid_mac
    - reserved_port
* ipv6address (in record:host)
    - conflict_types
    - discover_now_status
    - reserved_port
* ipv6fixedaddress
    - allow_telnet
    - cli_credentials
    - device_description
    - device_location
    - device_type
    - device_vendor
    - discover_now_status
    - reserved_interface
    - use_cli_credentials
    - discovered_data.iprg_no (search-only)
    - discovered_data.iprg_state (search-only)
    - discovered_data.iprg_type (search-only)
* ipv6network
    - discover_now_status
    - discovery_blackout_setting
    - port_control_blackout_setting
    - same_port_control_discovery_blackout
    - use_blackout_setting
* ipv6networkcontainer
    - discover_now_status
    - discovery_blackout_setting
    - port_control_blackout_setting
    - same_port_control_discovery_blackout
    - use_blackout_setting
* ipv6range
    - discover_now_status
    - discovery_blackout_setting
    - port_control_blackout_setting
    - same_port_control_discovery_blackout
    - use_blackout_setting
* lease
    - discovered_data.iprg_no (search-only)
    - discovered_data.iprg_state (search-only)
    - discovered_data.iprg_type (search-only)
* network
    - discover_now_status
    - discovery_blackout_setting
    - ipam_email_addresses
    - ipam_threshold_settings
    - ipam_trap_settings
    - port_control_blackout_setting
    - same_port_control_discovery_blackout
    - use_blackout_setting
    - use_ipam_email_addresses
    - use_ipam_threshold_settings
    - use_ipam_trap_settings
* networkcontainer
    - discover_now_status
    - discovery_blackout_setting
    - ipam_email_addresses
    - ipam_threshold_settings
    - ipam_trap_settings
    - port_control_blackout_setting
    - same_port_control_discovery_blackout
    - use_blackout_setting
    - use_ipam_email_addresses
    - use_ipam_threshold_settings
    - use_ipam_trap_settings
* range
    - discover_now_status
    - discovery_blackout_setting
    - port_control_blackout_setting
    - same_port_control_discovery_blackout
    - use_blackout_setting
* record:a
    - discovered_data.iprg_no (search-only)
    - discovered_data.iprg_state (search-only)
    - discovered_data.iprg_type (search-only)
* record:aaaa
    - discovered_data.iprg_no (search-only)
    - discovered_data.iprg_state (search-only)
    - discovered_data.iprg_type (search-only)
* record:host
    - allow_telnet
    - cli_credentials
    - device_description
    - device_location
    - device_type
    - device_vendor
* record:host_ipv4addr
    - discover_now_status
    - is_invalid_mac
    - reserved_interface
    - discovered_data.iprg_no (search-only)
    - discovered_data.iprg_state (search-only)
    - discovered_data.iprg_type (search-only)
* record:host_ipv6addr
    - discover_now_status
    - reserved_interface
    - discovered_data.iprg_no (search-only)
    - discovered_data.iprg_state (search-only)
    - discovered_data.iprg_type (search-only)
* record:ptr
    - discovered_data.iprg_no (search-only)
    - discovered_data.iprg_state (search-only)
    - discovered_data.iprg_type (search-only)
* scheduledtask
    - execution_details
    - execution_details_type
    - is_network_insight_task
    - predecessor_task
    - re_execute_task
    - task_type
* view
    - dnssec_negative_trust_anchors
* zone_auth
    - dns_integrity_enable
    - dns_integrity_frequency
    - dns_integrity_member
    - dns_integrity_verbose_logging
    - dnssec_keys
    - dnssec_ksk_rollover_date
    - dnssec_zsk_rollover_date
    - do_host_abstraction
    - ms_sync_disabled

Web API 1.6 also made the following changes to existing objects:

* csvimporttask added new values for the status field
* discovery:deviceinterface supported modify access
* fileop added the following new functions:
    - csv_snapshot_file
    - csv_uploaded_file

### Web API 1.7 (NIOS 6.12)

Web API version 1.7 introduced the following new object:

* member:dhcpproperties

Web API 1.7 added new fields to existing objects as follows:

* grid:dhcpproperties
    - options

### Web API 2.0 (NIOS 7.0)

Web API version 2.0 introduced the following new objects:

* admingroup
* adminuser
* dtc
* dtc:certificate
* dtc:lbdn
* dtc:monitor
* dtc:monitor:http
* dtc:monitor:icmp
* dtc:monitor:pdp
* dtc:monitor:sip
* dtc:monitor:tcp
* dtc:object
* dtc:pool
* dtc:server
* dtc:topology
* dtc:topology:label
* dtc:topology:rule
* grid:cloudapi
* grid:cloudapi:cloudstatistics
* grid:cloudapi:tenant
* grid:cloudapi:vmaddress
* grid:dns
* grid:maxminddbinfo
* grid:member:cloudapi
* grid:x509certificate
* member:dhcpproperties
* member:dns
* member:adsites:domain
* member:adsites:site
* record:dtclbdn

For further changes from NIOS 6.x see the _NIOS 7.0.2 Release Notes_.

## Appendix: Installing and using Python

The Python interpreter is open source software that is available on a
wide variety of systems either as part of the default installation (as
on Mac OS X and Linux) or as a free download (as on Microsoft
Windows). As shipped Python includes a number of standard _packages_
that add various functionality; you can install other packages to
supplement the standard Python distribution for your system. In
particular the Requests module used by the example code in this
document is not part of standard Python, and must be separately
installed.

Python is available in two major releases at this time, Python 2 and
Python 3. The example code in this document assumes the use of Python
2.7, the current minor release at the time of writing. Python 3
introduces various new features and changes to existing features, some
of which break compatibility with Python 2. Most current Python
applications target Python 2.7 or earlier, and in most systems the
`python` command will invoke Python 2.x (with `python3` used to invoke
Python 3).

### Python for Microsoft Windows

Python is not shipped with releases of Microsoft Windows. Pre-compiled
binaries for Python 2 and Python 3 are available from the Python
Software Foundation at `https://www.python.org/downloads/windows`. We
recommend using the latest Python 2.7 release (2.7.9 at the time of
writing). Modern 64-bit Windows systems should use the x86-64 MSI
installer; older systems should use the x86 MSI installer.

The Python installer is similar to other Windows installers. You can
install Python for all users on the system (default) or just for
yourself. By default Python is installed into the directory
`C:\Python27`, but you can choose to install it in a different
directory. Finally, you can choose to add the Python directory
containing `python.exe` to the system PATH variable. (This is _not_
done by default.)

You should also install the Setuptools software and the pip utility,
which allows you to easily install the Requests module and other
Python add-ons. For instructions see the document “Installing Python
on Windows” at `http://docs.python-guide.org`.

### Python for Mac OS X

Python is included as part of the base operating system on Mac OS
X. For example, the OS X Mavericks and Yosemite releases include
Python 2.7.6.

If you wish you can install the most current version of Python (2.7.9
at the time of writing) via the Homebrew system; this will also
install the Setuptools software and pip utility needed to install the
Requests module and other Python add-ons. For instructions see the
document “Installing Python on Mac OS X” at
`http://docs.python-guide.org`.

### Python for Linux

Python is included as part of the base operating system on almost all
common Linux distributions. For example, Ubuntu 14.04 LTS includes
Python 2.7.6.

If you wish you can install the most current version of Python (2.7.9
at the time of writing); you should definitely do this if your version
of Python is older than 2.6. For instructions see the document
“Installing Python on Linux” at `http://docs.python-guide.org`.

## Appendix: Using regular expressions with the Web API

*To be written.*
