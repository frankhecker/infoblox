import requests
import json
import csv

def ipv4addr_key(cidr):
    # Split CIDR string into network address string and prefix string.
    addr_str, prefix_str = cidr.split('/')

    # Split dotted decimal address into octets, convert to binary.
    addr_octets = [int(octet) for octet in addr_str.split('.')]
    addr_value = (256*256*256 * addr_octets[0] +
                  256*256 * addr_octets[1] +
                  256 * addr_octets[2] +
                  addr_octets[3])
    return addr_value

# Get IPv4 network objects (up to a max of 10000).
r = requests.get('https://gm.lab.fhecker.com/wapi/v1.0/network',
                 params={'_max_results': str(2000)},
                 auth=('fhecker', 'DavidB.Epileptic1'))
if r.status_code != requests.codes.ok:
    print r.text
    exitmsg = 'Error {} finding networks: {}'
    sys.exit(exitmsg.format(r.status_code, r.reason))
networks = r.json()

# For each network count all used IPv4 addresses (up to a max of 10000).
stats = {}
for network in networks:
    # Retrieve used ipv4address objects for this network.
#    print network['network'], network['network_view']
    r = requests.get('https://gm.lab.fhecker.com/wapi/v1.0/ipv4address',
                     params={'network': network['network'],
                             'network_view': network['network_view'],
                             'status': 'USED',
                             '_return_fields': '',
                             '_max_results': str(10000)},
                 auth=('fhecker', 'DavidB.Epileptic1'))
    if r.status_code != requests.codes.ok:
        print r.text
        exitmsg = 'Error {} finding IP addresses: {}'
        sys.exit(exitmsg.format(r.status_code, r.reason))
    ipv4addresses = r.json()

    # Count the number of ipv4address objects and save the count.
    used = len(ipv4addresses)
    prefix = int(network['network'].split('/')[1])
    size = 2**(32-prefix) - 2
    pct_used = round((100. * used) / size, 1)
    stats[network['network']] = [size, used, pct_used]

print stats

# Export the results.
with open('ipam-stats.csv', 'wb') as out_file:
    out_csv = csv.writer(out_file,
                         delimiter=',',
                         quotechar='"',
                         quoting=csv.QUOTE_MINIMAL)
    out_csv.writerow(['network',
                      'network_view',
                      'size',
                      'used',
                      'pct_used'])
    for network in sorted(stats.keys()):
        out_csv.writerow([network, stats[network][0], stats[network][1]])
