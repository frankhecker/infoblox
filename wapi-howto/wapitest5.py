import requests
import sys
r = requests.get('https://gm.lab.fhecker.com/wapi/v1.0/network?' +
                     'network=10.0.0.0/16' + '&' +
                     'network_view=default' + '&' +
                     '_return_fields=network,comment',
                 auth=('fhecker', 'DavidB.Epileptic1'))
if r.status_code != requests.codes.ok:
    errmsg = 'Error {} finding network views: {}'
    sys.exit(errmsg.format(r.status_code, r.reason))
nets = r.json()
if not nets:
    sys.exit('network not found')
elif len(nets) > 1:
    sys.exit('network not unique')
net = nets[0]
print net['network'], net.get('comment', '')

