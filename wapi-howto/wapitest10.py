import requests
import sys
try:
    r = requests.get('https://gm.lab.fhecker.com/wapi/v1.0.1/networkvieww',
                     auth=('fhecker', 'DavidB.Epileptic1'))
except:
    print sys.exc_info()
    errmsg = 'Exception {} finding network views'
    e = sys.exc_info()[0]
    sys.exit(errmsg.format(e))
if r.status_code != requests.codes.ok:
    print r.text
    errmsg = 'Error {} finding network views: {}'
    sys.exit(errmsg.format(r.status_code, r.reason))

print r.text
