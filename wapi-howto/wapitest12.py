import requests
import sys
import csv

r = requests.get('https://gm.lab.fhecker.com/wapi/v1.0/networkcontainer',
                 auth=('fhecker', 'DavidB.Epileptic1'))
if r.status_code != requests.codes.ok:
    print r.text
    exitmsg = 'Error {} finding network containers: {}'
    sys.exit(exitmsg.format(r.status_code, r.reason))
containers = r.json()

with open('network_containers.csv', 'wb') as out_file:
    out_csv = csv.writer(out_file,
                         delimiter=',',
                         quotechar='"',
                         quoting=csv.QUOTE_MINIMAL)
    out_csv.writerow(['Network Container',
                      'Network View',
                      'Comment'])
    for container in containers:
        out_csv.writerow([container['network'],
                          container['network_view'],
                          container.get('comment', '')])
