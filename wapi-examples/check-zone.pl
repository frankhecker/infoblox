#!/usr/bin/perl

# Load needed libraries
use WWW::Curl::Easy;
use JSON;
use Data::Dumper;

# Set parameters to access the NIOS WAPI.
my $url = 'https://gm.lab.fhecker.com/wapi/v1.0/'
my $acct = 'api-test'  # Userid with WAPI access
my $password = 'xxxxxx'
my $valid_cert = True  # False if GM uses self-signed certificate

# Set up the curl object for use.
my $curl = WWW::Curl::Easy->new;
#$curl->setopt(CURLOPT_VERBOSE,1); # turns on debug mode
$curl->setopt(CURLOPT_SSL_VERIFYPEER, 0); # disables SSL cert checking.
$curl->setopt(CURLOPT_USERPWD, "$acct:$passwd");
$curlcmd .= "?" . $return_fieldscmd . $networkfields;
$curl->setopt(CURLOPT_WRITEDATA, \$response_body); # sets up the location for the returned data to go.
$curl->setopt(CURLOPT_URL, $curlcmd);
my $response_body;
$curl->setopt(CURLOPT_WRITEDATA, \$response_body); # sets up the location for the returned data to go.

# And now, actually execute the request.
my $retcode= $curl->perform;


# We need to get the returned HTTP code. If that's between 400 and
# 599, we code some kind of error on our API request, so flag it as an
# error.

my $http_code = $curl->getinfo(CURLINFO_HTTP_CODE);
my $http_error = 0;
$http_error = $http_code if ($http_code >= 400 && $http_code < 600);

if ($retcode == 0 && $http_error == 0) {
    my $decoded_response = from_json($response_body);
    foreach my $network (@{$decoded_response}) {
	foreach my $key (sort keys %{$network}) {
            # We do a quick check on what kind of data is inside the
            # variable. If it's an array, we use a function to display
            # it.
	    if (ref(${$network}{$key}) eq 'ARRAY') {
		print $key , "\n";
		&print_array(${$network}{$key});
            # Ditto here if it's a hash
	    } elsif (ref(${$network}{$key}) eq 'HASH') {
		print $key , "\n";
		&print_hash(${$network}{$key});
	    } else {
		print $key , ": ", ${$network}{$key}, "\n";
	    }
	}
    }


