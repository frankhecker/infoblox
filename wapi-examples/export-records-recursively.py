#!/usr/local/bin/python

"""
Export selected DNS records from domain and all its subdomains.

This script looks for DNS records of a given type in a domain and all
subdomains under that domain, and then exports such records in a CSV
file for use with Microsoft Excel, etc. Note that the script assumes
that the highest-level domain is also a zone, but does not assume that
the subdomains are subzones.

To use this script

    Set the 'url' variable to contain the domain name or IP address of
    the grid master

    Set the 'id' variable to contain a userid with WAPI access to the
    grid master. (The script will prompt for the corresponding
    password when run.)

    If your grid master uses a TLS/SSL certificate from a commercial
    CA then set the variable 'valid_cert' to True.

    Set the 'top_zone' variable to the domain for which you want to
    export records.

    Set the 'top_zone_view' variable to the DNS view in which the
    'top_zone' value is located, or to the null string '' to use the
    default DNS view.

    Set the 'record_type' variable to the type of DNS record you want
    to export (e.g., 'A' or 'MX').

    Set the 'record_fields' variable to a list of the fields you want
    to export for the records, or to an empty list to export the set
    of default fields for the specified record type.

    Set to 'max_results' field to (the negative of) a number larger
    than the maximum number of records you expect to have returned
    from a single zone for the specified record type.

This script should work for NIOS 6.12 and later (WAPI 1.7 and later).
"""


# Import the required Python modules.
import requests
import json
import getpass
import sys
import csv


# Set parameters to access the NIOS WAPI.
url = 'https://gm.example.com/wapi/v1.7/'  # WAPI 1.7 == NIOS 6.12
id = 'api'  # Userid with WAPI access
valid_cert = False  # True if GM uses certificate from commercial CA

# Set parameters for the record retrieval.
top_zone = 'fhecker.com'  # Top zone to start retrieval
top_zone_view = ''  # Use default DNS view
record_type = 'MX'  # Return MX records
record_fields = ['name', 'mail_exchanger', 'preference', 'ttl']
max_results = -10000  # Assume no more than 10,000 records in one zone
csv_output = 'export-records-recursively.csv'


# Define supported record types and fields for each type.

# List default fields in order of desired presentation.
# TODO: Add NAPTR and TXT records, as well as host records.
default_fields = {
    'A': [
        'name',
        'ipv4addr',
        'view',
        ],
    'AAAA': [
        'name',
        'ipv6addr',
        'view',
        ],
    'CNAME': [
        'name',
        'canonical',
        'view',
        ],
    'MX': [
        'name',
        'mail_exchanger', 
        'preference',
        'view',
        ],
    'NAPTR': [
        'name',
        'order',
        'preference',
        'regexp',
        'replacement',
        'services',
        'view',
        ],
    'PTR': [
        'ptrdname',
        'ipv4addr',
        'view',
        ],
    'SRV': [
        'name',
        'port',
        'priority',
        'target',
        'weight',
        'view'
        ],
    'TXT': [
        'name',
        'text',
        'view'
        ],
    }

# List valid fields for each type of record.
valid_fields = {
    'A': [
        'comment',
        'disable',
        'dns_name',
        'ipv4addr',
        'name',
        'ttl',
        'use_ttl',
        'view',
        'zone',
        ],
    'AAAA': [
        'comment',
        'disable',
        'dns_name',
        'ipv6addr',
        'name',
        'ttl',
        'use_ttl',
        'view',
        'zone',
        ],
    'CNAME': [
        'canonical',
        'comment',
        'disable',
        'dns_canonical',
        'dns_name',
        'name',
        'ttl',
        'use_ttl',
        'view',
        'zone',
        ],
    'MX': [
        'comment',
        'disable',
        'dns_mail_exchanger',
        'dns_name',
        'mail_exchanger',
        'name',
        'preference',
        'ttl',
        'use_ttl',
        'view',
        'zone',
        ],
    'NAPTR': [
        'comment',
        'disable'
        'dns_name',
        'dns_replacement',
        'flags',
        # TODO: 'last_queried',
        'name',
        'order',
        'preference',
        'regexp',
        'replacement',
        'services',
        'ttl',
        'use_ttl',
        'view',
        'zone',
        ],
    'PTR': [
        'comment',
        'disable',
        'dns_name',
        'dns_ptrdname'
        'ipv4addr',
        'ipv6addr',
        'name',
        'ptrdname',
        'ttl',
        'use_ttl',
        'view',
        'zone',
        ],
    'SRV': [
        'comment',
        'disable',
        'dns_name',
        'dns_target',
        'name',
        'port',
        'priority',
        'target',
        'ttl',
        'use_ttl',
        'view',
        'weight',
        'zone',
        ],
    'TXT': [
        'comment',
        'disable',
        'dns_name',
        'name',
        'text',
        'ttl',
        'use_ttl',
        'view',
        'zone',
        ],
    }

# Define helper functions.
def init_wapi(url, id, pw, valid_cert):
    """Verify we can access grid, return grid object with auth cookie."""
    grid = {}
    r = requests.get(url + 'grid',
                 auth=(id, pw),
                 verify=valid_cert)
    if r.status_code != requests.codes.ok:
        print r.text
        exit_msg = 'Error {} accessing grid: {}'
        sys.exit(exit_msg.format(r.status_code, r.reason))
    grid['auth_cookie'] = r.cookies['ibapauth']
    return grid


def validate_request(type, fields):
    """Validate desired record type and fields, return list of fields."""
    if type not in valid_fields:
        exit_msg = 'Record type {} is invalid or not supported.'
        sys.exit(exit_msg.format(type))
    elif fields == []:
        return default_fields[type]
    else:
        for field in fields:
            if field not in valid_fields[type]:
                exit_msg = \
                    'Field {} is invalid or not supported for record type {}.'
                sys.exit(exit_msg.format(field, type))
        return fields


def get_zones(grid, zone, view):
    """Return list containing zone and all its subzones."""
    all_zones = get_all_zones(grid, view)
    if zone not in all_zones:
        exit_msg = '{} is not a zone.'
        sys.exit(exit_msg.format(zone))
    zones = [zone]
    for z in all_zones:
        if z.endswith('.' + top_zone):
            zones.append(z)
    return zones


def get_all_zones(grid, view):
    """Return list of all forward- or reverse-mapping zones."""

    req_cookies = {'ibapauth': grid['auth_cookie']}
    # TODO: Specify network view for zone search.
    req_params = {}
    if view:
        req_params['view'] = view
    r = requests.get(url + 'zone_auth',
                     params=req_params,
                     cookies=req_cookies,
                     verify=valid_cert)
    if r.status_code != requests.codes.ok:
        print r.text
        exit_msg = 'Error {} finding zones: {}'
        sys.exit(exit_msg.format(r.status_code, r.reason))

    # Loop through results and save zone names in a list.
    results = r.json()
    zones = []
    for result in results:
        zones.append(result['fqdn'])
    return zones


def get_rrsets_in_zones(grid, zones, view, type, fields):
    """Return dictionary of rrsets of specified type in the zones."""
    rrsets = {}
    for zone in zones:
        get_rrsets_in_zone(grid, zone, view, type, fields, rrsets)
    return rrsets


def get_rrsets_in_zone(grid, zone, view, type, fields, rrsets):
    """Get all rrsets of given type in zone, add to rrsets."""
    req_cookies = {'ibapauth': grid['auth_cookie']}
    # TODO: Specify network view for record search.
    fields_to_return = ','.join(validate_request(type, fields))
    req_params = {
        'zone': zone,
        '_return_fields': fields_to_return,
        '_max_results': str(max_results),
        }
    if view:
        req_params['view'] = view
    r = requests.get(url + 'record:' + type.lower(),
                     params=req_params,
                     cookies=req_cookies,
                     verify=valid_cert)
    if r.status_code != requests.codes.ok:
        print r.text
        exit_msg = 'Error {} retrieving records from zone {}: {}'
        sys.exit(exit_msg.format(r.status_code, zone, r.reason))

    # Loop through results and add to rrsets dictionary by FQDN.
    # Since there may be multiple DNS records in each rrset we store
    # each entry in the dictionary as a list.
    results = r.json()
    if type == 'PTR':
        record_id = 'ptrdname'  # 'name' is not always present for PTR
    else:
        record_id = 'name'
    for result in results:
        if result[record_id] not in rrsets:
            rrsets[result[record_id]] = []
        rrsets[result[record_id]].append(result)


def export_rrsets(csv_output, rrsets, fields):
    """Export DNS records to a file in CSV format."""
    with open(csv_output, 'wb') as out_file:
        out_csv = csv.writer(out_file,
                             delimiter=',',
                             quotechar='"',
                             quoting=csv.QUOTE_MINIMAL)

        # Write header row with field names.
        out_csv.writerow(fields)

        # Write one data row per record in an rrset.
        for fqdn in sorted(rrsets.keys(), key=fqdn_sort_key):
            for record in rrsets[fqdn]:
                out_csv.writerow(format_row(record, fields))


def format_row(record, fields):
    """Convert a DNS record object to a list for CSV output."""
    row = []
    for field in fields:
        # A field may not appear in all records (e.g., if inherited).
        if field in record:
            row.append(str(record[field]))
        else:
            row.append('')
    return row


def fqdn_sort_key(fqdn):
    """Given an FQDN, return a string with domains reversed for sorting."""
    return '.'.join(reversed(fqdn.split('.')))


def main():
    """Find top-level zone and export records in it and subzones.""" 

    # Prompt for the API user password.
    pw = getpass.getpass('Password for user ' + id + ': ')

    # If running on Windows avoid error due to a self-signed cert.
    if sys.platform.startswith('win') and not valid_cert:
        requests.packages.urllib3.disable_warnings()

    # Initialize the WAPI.
    grid = init_wapi(url, id, pw, valid_cert)

    # Find top-level zone and its subzones in the specified DNS view.
    zones = get_zones(grid, top_zone, top_zone_view)

    # Look for records of the desired type in the zone and its
    # subzones and then export them as a CSV file.
    desired_fields = validate_request(record_type, record_fields)
    rrsets = get_rrsets_in_zones(grid,
                                 zones,
                                 top_zone_view,
                                 record_type,
                                 desired_fields)
    export_rrsets(csv_output, rrsets, desired_fields)

if __name__ == '__main__':
    main()
