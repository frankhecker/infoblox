#!/usr/bin/python
# Create a network container and networks within it.

"""
Create a network container and networks within it.

This script should work on NIOS 6.6 and later (WAPI 1.0 and later).
"""

# Import the required Python modules.
import sys
import requests
import json

# Set parameters to access the Infoblox API for your own grid master.
url = 'https://gm.lab.fhecker.com/wapi/v1.0/'
id = 'fhecker'
pw = 'xxxxxx'
valid_cert = True  # False if self-signed certificate

# Network container, list of networks to create within it, and network view.
net_container = '192.168.250.0/24'
nets = ['192.168.250.0/26', '192.168.250.64/26',
        '192.168.250.128/26', '192.168.250.192/26']
net_view = 'default'

# Create the network container.
container_data = {'network': net_container, 'network_view': net_view}
r = requests.post(url + 'networkcontainer',
                  data=json.dumps(container_data),
                  auth=(id, pw),
                  verify=valid_cert)
if r.status_code != requests.codes.created:
    error_msg = 'Error {} creating network container {}: {}'
    sys.exit(error_msg.format(r.status_code, r.reason, net_container))
else:
    success_msg = 'Created network container {}'
    print success_msg.format(net_container)

# Create subnets within the network container.
for net in nets:
    net_data = {'network': net, 'network_view': net_view}
    r = requests.post(url + 'network',
                      data=json.dumps(net_data),
                      auth=(id, pw),
                      verify=valid_cert)
    if r.status_code != requests.codes.created:
        error_msg = 'Error {} ({}) creating network {}'
        sys.exit(error_msg.format(r.status_code, r.reason, net))
    else:
        success_msg = 'Created network {}'
        print success_msg.format(net)
