#!/usr/local/bin/python

"""
List DNS records for all zones in all DNS views in all network views.

To use this script change the 'url' variable to contain the domain
name or IP address of the grid master, and change the 'id' variable to
contain a userid with WAPI access to the grid master. (The script will
prompt for the corresponding password when run.)

If your grid master uses a self-signed TLS/SSL certificate then set
the variable 'valid_cert' to False. If your grid contains more than
5,000 CNAME records then set the variable 'max_results' to the
(negative of the) maximum number of CNAME records to return.

This script should work for NIOS 6.6 and later (WAPI 1.0 and later).
"""

# Import the required Python modules.
import requests
import json
import csv
import getpass
import sys

# Set parameters to access the NIOS WAPI.
url = 'https://gm.lab.fhecker.com/wapi/v2.0/'
id = 'api-test'  # Userid with WAPI access
valid_cert = True  # False if GM uses self-signed certificate

# Prompt for the API user password.
pw = getpass.getpass('Password for user ' + id + ': ')

# Retrieve all DNS views in all network views.
r = requests.get(url + 'view',
                 auth=(id, pw),
                 verify=valid_cert)
if r.status_code != requests.codes.ok:
    print r.text
    exit_msg = 'Error {} finding views: {}'
    sys.exit(exit_msg.format(r.status_code, r.reason))
results = r.json()

# Keep track of (network view, DNS view) pairs.
views = []
for result in results:

    views.append(result["name"])

# Retrieve all zones in each view (up to a max of 5000 per view).
dns_view = 'internal'
max_results = -5000
req_params = {'view': dns_view,
              '_max_results': str(max_results)}
r = requests.get(url + 'record:cname',
                 params=req_params,
                 auth=(id, pw),
                 verify=valid_cert)
if r.status_code != requests.codes.ok:
    print r.text
    exit_msg = 'Error {} finding CNAME records: {}'
    sys.exit(exit_msg.format(r.status_code, r.reason))
results = r.json()

# For each CNAME record keep track of the canonical name pointed
# to by that record, by adding the name to the Python set.
# Sort the resulting list by name.
canonicals = set()
for result in results:
    canonicals.add(result['canonical'])
for canonical in sorted(canonicals):
    print canonical
