#!/usr/local/bin/python
# Create a host record using the next available IP in a network.

"""
Create a host record using the next available IP in a network.

To use this script change the 'url' variable to contain the domain
name or IP address of the grid master, and change the 'id' variable to
contain a userid with WAPI access to the grid master. (The script will
prompt for the corresponding password when run.)

If your grid master uses a self-signed TLS/SSL certificate then set
the variable 'valid_cert' to False. If your grid contains more than
5,000 CNAME records then set the variable 'max_results' to the
(negative of the) maximum number of CNAME records to return.

This script should work for NIOS 6.8.2 and later (WAPI 1.2.1 and
later).
"""

# Import the required Python modules.
import requests
import json
import csv
import getpass
import sys

# Set parameters to access the NIOS WAPI.
url = 'https://gm.lab.fhecker.com/wapi/v1.2.1/'
id = 'api-test'  # Userid with WAPI access
valid_cert = True  # False if GM uses self-signed certificate

# Prompt for the API user password.
pw = getpass.getpass('Password for user ' + id + ': ')

# Create 
dns_view = 'internal'
network = '192.168.201.0/24'
req_data = {'view': dns_view,
              '_max_results': str(max_results)}
r = requests.post(url + 'record:host',
                 data=json.dumps(req_data),
                 auth=(id, pw),
                 verify=valid_cert)
if r.status_code != requests.codes.ok:
    print r.text
    exit_msg = 'Error {} finding CNAME records: {}'
    sys.exit(exit_msg.format(r.status_code, r.reason))
results = r.json()

# For each CNAME record keep track of the canonical name pointed
# to by that record, by adding the name to the Python set.
# Sort the resulting list by name.
canonicals = set()
for result in results:
    canonicals.add(result['canonical'])
for canonical in sorted(canonicals):
    print canonical
