#!/usr/local/bin/python
# Export all network containers and networks.

"""
Export all network containers and networks in CSV format.

To use this script change the 'url' variable to contain the domain
name or IP address of the grid master, and change the 'id' variable to
contain a userid with WAPI access to the grid master. (The script will
prompt for the corresponding password when run.)

If your grid master uses a TLS/SSL certificate from a commercial CA
then set the variable 'valid_cert' to True. If your grid contains more
than 5,000 networks (or network containers) then set the variable
'max_results' to the (negative of the) number of networks (or network
containers) to return.

This script should work for NIOS 6.6 and later (WAPI 1.0 and later).
"""

# Import the required Python modules.
import requests
import json
import csv
import getpass
import sys

def ipv4addr_key(network_tuple):
    """
    Return a character string to use for sorting networks by address.

    Args:
      network_tuple (tuple): A tuple with a string containing an IPv4
        network address in CIDR format as the first element.

    Returns:
      str: A 14-character string, with the first 12 characters
      containing the zero-filled integer value for the 32-bit network
      address, and the last 2 characters containing the zero-filled
      integer value for the CIDR prefix.
    """

    # Split CIDR string into network address string and prefix string.
    cidr = network_tuple[0]
    addr_str, prefix_str = cidr.split('/')

    # Split dotted decimal address into octets, convert to binary.
    addr_octets = [int(octet) for octet in addr_str.split('.')]
    addr_value = (256*256*256 * addr_octets[0] +
                  256*256 * addr_octets[1] +
                  256 * addr_octets[2] +
                  addr_octets[3])

    # Concatenate address value and prefix value to form sort key.
    prefix_value = int(prefix_str)
    key_str = '{:0>12d}/{:0>2d}'.format(addr_value, prefix_value)
    return key_str


# Set parameters to access the NIOS WAPI.
url = 'https://gm.example.com/wapi/v1.0/'
id = 'api'  # Userid with WAPI access
valid_cert = False  # True if GM uses certificate from commercial CA

# Prompt for the API user password.
pw = getpass.getpass('Password for user ' + id + ': ')

# Retrieve all network objects (up to a max of 5000).
network_view = 'default'
max_results = -5000
req_params = {'network_view': network_view,
              '_max_results': str(max_results)}
r = requests.get(url + 'network',
                 params=req_params,
                 auth=(id, pw),
                 verify=valid_cert)
if r.status_code != requests.codes.ok:
    print r.text
    exit_msg = 'Error {} finding networks: {}'
    sys.exit(exit_msg.format(r.status_code, r.reason))
results = r.json()

# Save the authentication cookie for use in the next request.
ibapauth_cookie = r.cookies['ibapauth']

# Retrieve all network container objects (up to a max of 5000).
# Use the ibapauth cookie to authenticate instead of userid/password.
request_cookies = {'ibapauth': ibapauth_cookie}
network_view = 'default'
max_results = -5000
req_params = {'network_view': network_view,
              '_max_results': str(max_results)}
r = requests.get(url + 'networkcontainer',
                 params=req_params,
                 cookies=request_cookies,
                 verify=valid_cert)
if r.status_code != requests.codes.ok:
    print r.text
    exit_msg = 'Error {} finding network containers: {}'
    sys.exit(exit_msg.format(r.status_code, r.reason))
results.extend(r.json())

# For each network or container keep track of the network address and
# any comment and add the resulting tuple to the list of networks.
# Sort the resulting list by network address and prefix.
networks = []
for result in results:
    networks.append((result['network'], result.get('comment', '')))
networks.sort(key=ipv4addr_key)

# Export the results in CSV format.
with open('export-network-hierarchy.csv', 'wb') as out_file:
    out_csv = csv.writer(out_file,
                         delimiter=',',
                         quotechar='"',
                         quoting=csv.QUOTE_MINIMAL)
    out_csv.writerow(['Network','Comment'])
    for network in networks:
        out_csv.writerow(list(network))
