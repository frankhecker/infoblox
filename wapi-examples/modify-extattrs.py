#!/usr/bin/python
# Modify extensible attributes on an object.

# Import the required Python modules.
import sys
import requests
import json

# Set parameters to access the Infoblox API for your own grid master.
url = 'https://gm.lab.fhecker.com/wapi/v1.7/'
id = 'fhecker'
pw = 'xxxxxx'
valid_cert = True  # False if self-signed certificate

# Look for a specific network in the default network view, and
# return its extensible attributes.
network_to_change = '192.168.202.0'
network_view = 'default'
search_params = {'network': network_to_change,
                 'network_view': network_view,
                 '_return_fields': 'extattrs'}
r = requests.get(url + 'network',
                 params=search_params,
                 auth=(id, pw),
                 verify=valid_cert)
if r.status_code != requests.codes.ok:
    sys.exit('Error {} finding network: {}'.format(r.status_code, r.reason))

# Make sure we found the one network we were looking for.
networks = r.json()
if not networks:
    sys.exit('Network {} not found'.format(network_to_change))
else if len(networks) > 1:
    sys.exit('Network {} not unique'.format(network_to_change))

# Extract the network's object reference and current extensible
# attribute values (if any).
network = networks[0]
ref = network.get('_ref')
if not ref:
    sys.exit('No object reference for network {}'.format(network_to_change))
attributes = network.get('extattrs', {})

# Add the new extensible attributes value. If the extensible attribute
# in question was already present this will overwrite its value,
# otherwise it will add a new extensible attribute.
attributes['State'] = 'Maryland'

#
