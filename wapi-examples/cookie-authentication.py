#!/usr/local/bin/python

"""
Show use of ipapauthcookie for authentication of multiple requests.

To use this script change the 'url' variable to contain the domain
name or IP address of the grid master, and change the 'id' variable to
contain a userid with WAPI access to the grid master. (The script will
prompt for the corresponding password when run.)

If your grid master uses a self-signed TLS/SSL certificate then set
the variable 'valid_cert' to False. If your grid contains more than
5,000 CNAME records then set the variable 'max_results' to the
(negative of the) maximum number of CNAME records to return.

This script should work for NIOS 6.6 and later (WAPI 1.0 and later).
"""

# Import the required Python modules.
import requests
import json
import getpass
import sys

# Set parameters to access the NIOS WAPI.
url = 'https://gm.lab.fhecker.com/wapi/v2.0/'
id = 'api-test'  # Userid with WAPI access
valid_cert = True  # False if GM uses self-signed certificate

# Prompt for the API user password.
pw = getpass.getpass('Password for user ' + id + ': ')

# Retrieve all network views.
r = requests.get(url + 'networkview',
                 auth=(id, pw),
                 verify=valid_cert)
if r.status_code != requests.codes.ok:
    print r.text
    exit_msg = 'Error {} finding network views: {}'
    sys.exit(exit_msg.format(r.status_code, r.reason))
results = r.json()

# Save the authentication cookie for use in subsequent requests.
ibapauth_cookie = r.cookies['ibapauth']
print 'Authentication cookie: ', ibapauth_cookie

# Print the names of the network views.
print 'Network views'
for result in results:
    print result['name']

# Retrieve all DNS views. This time use the ibapauth cookie to
# authenticate instead of a userid and password.
request_cookies = {'ibapauth': ibapauth_cookie}
r = requests.get(url + 'view',
                 cookies=request_cookies,
                 verify=valid_cert)
if r.status_code != requests.codes.ok:
    print r.text
    exit_msg = 'Error {} finding DNS views: {}'
    sys.exit(exit_msg.format(r.status_code, r.reason))
results = r.json()

# Print the names of the DNS views.
print 'DNS views'
for result in results:
    print result['name']
