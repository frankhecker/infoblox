#!/usr/local/bin/python

"""
Report duplicated hostnames (one hostname matches multiple addresses).

This script looks for various cases where duplicate records may have
been created in error, so that one hostname is associated with
multiple network addresses. This includes:

  * host records with more than one IPv4 address
  * multiple A records with the same FQDN but different IPv4 addresses
  * multiple PTR records with different IPv4 addresses but the same FQDN

To use this script change the 'url' variable to contain the domain
name or IP address of the grid master, and change the 'id' variable to
contain a userid with WAPI access to the grid master. (The script will
prompt for the corresponding password when run.)

If your grid master uses a TLS/SSL certificate from a commercial CA
then set the variable 'valid_cert' to True.

If your grid contains more than 5,000 host records, A records, or PTR
records then set the variable 'max_results' to the (negative of the)
number of records addresses to return.

This script should work for NIOS 6.12 and later (WAPI 1.7 and later).
"""


# Import the required Python modules.
import requests
import json
import csv
import getpass
import sys


# Set parameters to access the NIOS WAPI.
url = 'https://gm.example.com/wapi/v1.7/'  # WAPI 1.7 == NIOS 6.12
id = 'api'  # Userid with WAPI access
valid_cert = False  # True if GM uses certificate from commercial CA

# Prompt for the API user password.
pw = getpass.getpass('Password for user ' + id + ': ')

# If running on Windows avoid error due to a self-signed cert.
if sys.platform.startswith('win') and not valid_cert:
    requests.packages.urllib3.disable_warnings()

# Set maximum number of records to return from API query.
max_results = -5000


# We consult three sources of data that associate hostnames with IPv4
# addresses: host records, A records, and PTR records. We use these
# sources to build a database (instantiated as a Python dictionary)
# that maps each FQDN to one or more IPv4 addresses.
fqdn_data = {}


# Retrieve (up to 5000) host records in the default DNS view.
req_params = {'_return_fields': 'name,ipv4addrs',
              '_max_results': str(max_results)}              
r = requests.get(url + 'record:host',
                 params=req_params,
                 auth=(id, pw),
                 verify=valid_cert)
if r.status_code != requests.codes.ok:
    print r.text
    exit_msg = 'Error {} finding host records: {}'
    sys.exit(exit_msg.format(r.status_code, r.reason))

# Save the returned host record objects for analysis.
host_records = r.json()

# Save the authentication cookie for use in the next request.
ibapauth_cookie = r.cookies['ibapauth']

# Record mappings of FQDNs to IPv4 addresses.
for host_record in host_records:
    if 'ipv4addrs' in host_record:  # ignore IPv6-only host records
        fqdn = host_record['name'].lower()
        if fqdn not in fqdn_data:
            fqdn_data[fqdn] = []
        for ipv4addr in host_record['ipv4addrs']:
            addr = ipv4addr['ipv4addr']
            if addr not in fqdn_data[fqdn]:
                fqdn_data[fqdn].append(addr)


# Retrieve (up to 5000) A records in the default DNS view.
# Use the ibapauth cookie to authenticate instead of userid/password.
req_params = {'_return_fields': 'name,ipv4addr',
              '_max_results': str(max_results)}              
req_cookies = {'ibapauth': ibapauth_cookie}
r = requests.get(url + 'record:a',
                 params=req_params,
                 cookies=req_cookies,
                 verify=valid_cert)
if r.status_code != requests.codes.ok:
    print r.text
    exit_msg = 'Error {} finding A records: {}'
    sys.exit(exit_msg.format(r.status_code, r.reason))

# Save the returned A record objects for analysis.
a_records = r.json()

# Record mappings of FQDNs to IPv4 addresses.
for a_record in a_records:
    fqdn = a_record['name'].lower()
    if fqdn not in fqdn_data:
        fqdn_data[fqdn] = []
    addr = a_record['ipv4addr']
    if addr not in fqdn_data[fqdn]:
        fqdn_data[fqdn].append(addr)


# Retrieve (up to 5000) PTR records in the default DNS view.
# Use the ibapauth cookie to authenticate instead of userid/password.
req_params = {'_max_results': str(max_results),
              '_return_fields': 'ptrdname,ipv4addr'}
req_cookies = {'ibapauth': ibapauth_cookie}
r = requests.get(url + 'record:ptr',
                 params=req_params,
                 cookies=req_cookies,
                 verify=valid_cert)
if r.status_code != requests.codes.ok:
    print r.text
    exit_msg = 'Error {} finding PTR records: {}'
    sys.exit(exit_msg.format(r.status_code, r.reason))

# Save the returned PTR record objects for analysis.
ptr_records = r.json()

# Record mappings of FQDNs to IPv4 addresses.
for ptr_record in ptr_records:
    fqdn = ptr_record['ptrdname'].lower()
    if fqdn != 'localhost' and 'ipv4addr' in ptr_record:  # IPv4 records only
        if fqdn not in fqdn_data:
            fqdn_data[fqdn] = []
        addr = ptr_record['ipv4addr']
        if addr not in fqdn_data[fqdn]:
            fqdn_data[fqdn].append(addr)


# Look for FQDNs mapped to more than one IPv4 address.
dup_data = {}
for fqdn in fqdn_data:
    if len(fqdn_data[fqdn]) > 1:
        dup_data[fqdn] = fqdn_data[fqdn]


# Export the results in CSV format.
with open('duplicate-hostnames.csv', 'wb') as out_file:
    out_csv = csv.writer(out_file,
                         delimiter=',',
                         quotechar='"',
                         quoting=csv.QUOTE_MINIMAL)
    # Export these columns.
    header_row = ['FQDN', 'Addresses']
    out_csv.writerow(header_row)
    # Export one row for each FQDN.
    for fqdn in dup_data:
        out_csv.writerow([fqdn, ','.join(dup_data[fqdn])])
