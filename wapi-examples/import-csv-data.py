#!/usr/local/bin/python

"""
Import CSV data from a file.

This script uploads a CSV-formatted file (in Infoblox CSV format) into
the grid and starts a CSV import task.

To use this script change the 'url' variable to contain the domain
name or IP address of the grid master, and change the 'id' variable to
contain a userid with WAPI access to the grid master. (The script will
prompt for the corresponding password when run.)

If your grid master uses a TLS/SSL certificate from a commercial CA
then set the variable 'valid_cert' to True. If your grid contains more
than 5,000 networks or more than 5,000 used addresses in a given
network then set the variable 'max_results' to the (negative of the)
number of networks or addresses to return.

This script should work for NIOS 6.12 and later (WAPI 1.7 and later).
"""


# Import the required Python modules.
import requests
import json
import csv
import getpass
import sys
import os


# Set parameters to access the NIOS WAPI.
url = 'https://gm.example.com/wapi/v1.7/'
id = 'api'  # Userid with WAPI access
valid_cert = False  # True if GM uses certificate from commercial CA


# Helper functions.
def sanitize_filename(pathname):
    """Return sanitized filename without path information."""

    # Get the base filename without the directory path, convert dashes
    # to underscores, and get rid of other special characters.
    filename = ''
    for c in os.path.basename(pathname):
        if c == '-':
            c = '_'
        if c.isalnum() or c == '_' or c == '.':
            filename += c
    return filename


# Prompt for the API user password.
pw = getpass.getpass('Password for user ' + id + ': ')

# If running on Windows avoid error due to a self-signed cert.
if sys.platform.startswith('win') and not valid_cert:
    requests.packages.urllib3.disable_warnings()

# The CSV file we want to import (in the current directory).
csv_data = '/Users/fhecker/csv-data.csv'

# Initiate a file upload operation, providing a filename (with
# alphanumeric, underscore, or periods only) for the CSV job manager.
req_params = {'filename': sanitize_filename(csv_data)}
r = requests.post(url + 'fileop?_function=uploadinit',
                  params=req_params,
                  auth=(id, pw),
                  verify=valid_cert)
if r.status_code != requests.codes.ok:
    print r.text
    exit_msg = 'Error {} initiating upload: {}'
    sys.exit(exit_msg.format(r.status_code, r.reason))
results = r.json()

# Save the authentication cookie for use in subsequent requests.
ibapauth_cookie = r.cookies['ibapauth']

# Save the returned URL and token for subsequent requests.
upload_url = results['url']
upload_token = results['token']


# Upload the data in the CSV file.

# Specify a file handle for the file data to be uploaded.
req_files = {'filedata': open(csv_data,'r')}

# Specify the name of the file (not used?).
req_params = {'name': sanitize_filename(csv_data)}

# Use the ibapauth cookie to authenticate instead of userid/password.
req_cookies = {'ibapauth': ibapauth_cookie}

# Perform the actual upload. (NOTE: It does NOT return JSON results.)
r = requests.post(upload_url,
                  params=req_params,
                  files=req_files,
                  cookies=req_cookies,
                  verify=valid_cert)
if r.status_code != requests.codes.ok:
    print r.text
    exit_msg = 'Error {} uploading file: {}'
    sys.exit(exit_msg.format(r.status_code, r.reason))


# Initiate the actual import task.
req_params = {'token': upload_token,
              'doimport': True,
              'on_error': 'STOP',
              'operation': 'INSERT',
              'update_method': 'OVERRIDE'}
r = requests.post(url + 'fileop?_function=csv_import',
                  params=req_params,
                  cookies=req_cookies,
                  verify=valid_cert)
if r.status_code != requests.codes.ok:
    print r.text
    exit_msg = 'Error {} starting CSV import: {}'
    sys.exit(exit_msg.format(r.status_code, r.reason))
results = r.json()

# Record cvsimporttask object reference for possible future use.
csvimporttask = results['csv_import_task']['_ref']
print csvimporttask
