#!/usr/local/bin/python

"""
Update a DNS zone in grid by pulling data from an external nameserver.

This script pulls zone data from a nameserver via zone transfer and
then updates the corresponding zone on an Infoblox grid by adding,
deleting, or modifying A or PTR records as necessary. (This differs
from normal zone transfer in allowing you to update records in an
existing zone on the grid while preserving records already present.)

To use this script

    Set the 'url' variable to contain the domain name or IP address of
    the grid master

    Set the 'id' variable to contain a userid with WAPI access to the
    grid master. (The script will prompt for the corresponding
    password when run.)

    If your grid master uses a TLS/SSL certificate from a commercial
    CA then set the variable 'valid_cert' to True.

    Set the 'nameserver' variable to the IP address of the nameserver
    from which you want to get zone data.

    Set the 'zone' variable to the name of the zone to transfer.

This script should work for NIOS 6.12 and later (WAPI 1.7 and later).
"""


# Import the required Python modules.
import requests
import dns.query
import dns.zone
import json
import getpass
import sys
import time


# Set parameters to access the NIOS WAPI.
url = 'https://gm.example.com/wapi/v1.7/'  # WAPI 1.7 == NIOS 6.12
id = 'api'  # Userid with WAPI access
valid_cert = False  # True if GM uses certificate from commercial CA

# Set parameters for the zone transfer.
nameserver = '45.55.250.132'
zone = 'xfrtest.com'


# Define helper functions.
def is_reverse_zone(zone):
    """Return True if zone is a reverse mapping zone."""
    return zone.endswith('in-addr.arpa') | zone.endswith('ip6.arpa')


def xfr_zone(nameserver, zone):
    """Transfer zone, return dictionary of A or PTR records."""
    z = dns.zone.from_xfr(dns.query.xfr(nameserver, zone))
    records = {}
    if is_reverse_zone(zone):
        # PTR records become 2-tuples (fqdn, ttl) keyed by address.
        for (name, ttl, rdata) in z.iterate_rdatas('PTR'):
            records[name.to_text()] = (rdata.target.to_text(), ttl)
    else:
        # A records become 2-tuples (address, ttl) keyed by fqdn.
        for (name, ttl, rdata) in z.iterate_rdatas('A'):
            records[name.to_text()] = (rdata.address.encode("ascii"), ttl)
    return records


def diff_zone(prev_records, records):
    """Compare zone records, return dicts of records to add/delete/modify."""
    to_add = {}
    to_delete = {}
    to_modify ={}

    # First find all records that were deleted.
    for r in prev_records:
        if r not in records:
            to_delete[r] = prev_records[r]

    # Then find all records that were added or modified.
    for r in records:
        if r not in prev_records:
            to_add[r] = records[r]
        elif records[r] != prev_records[r]:
            to_modify[r] = records[r]

    return to_add, to_delete, to_modify


def init_wapi(url, id, pw, valid_cert):
    """Verify we can access the grid, return auth cookie."""
    r = requests.get(url + 'grid',
                 auth=(id, pw),
                 verify=valid_cert)
    if r.status_code != requests.codes.ok:
        print r.text
        exit_msg = 'Error {} accessing grid: {}'
        sys.exit(exit_msg.format(r.status_code, r.reason))
    return r.cookies['ibapauth']


def add_a_record(fqdn, target, ttl, auth_cookie):
    """Add A record, ignoring errors if already present."""

    # Attempt to add new record using values provided.
    req_data = {'name': fqdn,
                'ipv4addr': target,
                'ttl': ttl}
    req_cookies = {'ibapauth': auth_cookie}
    r = requests.post(url + 'record:a',
                      data=json.dumps(req_data),
                      cookies=req_cookies,
                      verify=valid_cert)
    if r.status_code != requests.codes.created:
        # Unpack JSON text returned by request to determine error.
        rt = json.loads(r.text)
        if not rt['text'].endswith('already exists.'):
            exit_msg = 'Error {} creating A record: {}'
            sys.exit(exit_msg.format(r.status_code, r.reason))


def delete_a_record(fqdn, auth_cookie):
    """Delete A record from zone if present."""

    # Look for record in grid database.
    req_data = {'name': fqdn}
    req_cookies = {'ibapauth': auth_cookie}
    r = requests.get(url + 'record:a',
                     data=json.dumps(req_data),
                     cookies=req_cookies,
                     verify=valid_cert)
    if r.status_code != requests.codes.ok:
        print r.status_code
        print r.text
        exit_msg = 'Error {} finding A record: {}'
        sys.exit(exit_msg.format(r.status_code, r.reason))

    # Delete record using its reference object.
    ref = r.json()[0]['_ref']
    r = requests.delete(url + ref,
                        cookies=req_cookies,
                        verify=valid_cert)
    if r.status_code != requests.codes.ok:
        print r.status_code
        print r.text
        exit_msg = 'Error {} deleting A record: {}'
        sys.exit(exit_msg.format(r.status_code, r.reason))


def modify_a_record(fqdn, target, ttl, auth_cookie):
    """Modify A record in zone, adding if not already present."""

    # Look for record in grid database.
    req_data = {'name': fqdn}
    req_cookies = {'ibapauth': auth_cookie}
    r = requests.get(url + 'record:a',
                     data=json.dumps(req_data),
                     cookies=req_cookies,
                     verify=valid_cert)
    if r.status_code != requests.codes.ok:
        print r.status_code
        print r.text
        exit_msg = 'Error {} finding A record: {}'
        sys.exit(exit_msg.format(r.status_code, r.reason))

    # Modify record using its reference object.
    ref = r.json()[0]['_ref']
    req_data = {'ipv4addr': target,
                'ttl': ttl}
    r = requests.put(url + ref,
                     data=json.dumps(req_data),
                     cookies=req_cookies,
                     verify=valid_cert)
    if r.status_code != requests.codes.ok:
        print r.status_code
        print r.text
        exit_msg = 'Error {} modifying A record: {}'
        sys.exit(exit_msg.format(r.status_code, r.reason))


def add_records(records, zone, auth_cookie):
    """Add records to zone, ignoring errors if already present."""
    for record in records:
        fqdn = record + '.' + zone
        target = records[record][0]
        ttl = records[record][1]
        if is_reverse_zone(zone):
            print 'reverse zone'
        else:
            add_a_record(fqdn, target, ttl, auth_cookie)


def delete_records(records, zone, auth_cookie):
    """Delete records from zone."""
    for record in records:
        fqdn = record + '.' + zone
        if is_reverse_zone(zone):
            print 'reverse zone'
        else:
            delete_a_record(fqdn, auth_cookie)


def modify_records(records, zone, auth_cookie):
    """Modify records in zone."""
    for record in records:
        fqdn = record + '.' + zone
        target = records[record][0]
        ttl = records[record][1]
        if is_reverse_zone(zone):
            print 'reverse zone'
        else:
            modify_a_record(fqdn, target, ttl, auth_cookie)


def main():
    """Loop doing zone transfer and updating the grid."""

    # Prompt for the API user password.
    pw = getpass.getpass('Password for user ' + id + ': ')

    # If running on Windows avoid error due to a self-signed cert.
    if sys.platform.startswith('win') and not valid_cert:
        requests.packages.urllib3.disable_warnings()

    # Initialize the WAPI.
    auth_cookie = init_wapi(url, id, pw, valid_cert)

    # Periodically poll for changes.
    prev_records = {}
    while True:
        records = xfr_zone(nameserver, zone)
        (to_add, to_delete, to_modify) = diff_zone(prev_records, records)
        print zone, 'changes (add/delete/modify)'
        print to_add, to_delete, to_modify
        add_records(to_add, zone, auth_cookie)
        delete_records(to_delete, zone, auth_cookie)
        modify_records(to_modify, zone, auth_cookie)
        prev_records = records
        time.sleep(30)


if __name__ == '__main__':
    main()
