#!/usr/local/bin/python
# Export count of used IP addresses and other stats for all networks.

"""
Find all networks and export used/% used IP addresses for each as CSV.

This script retrieves all IPv4 networks in the default network view,
and for each network calculates the number of addresses used, the size
of the network, and the percent utilization. It exports this
information to a CSV file along with the network address and netmask
and selected extensible attributes.

To use this script change the 'url' variable to contain the domain
name or IP address of the grid master, and change the 'id' variable to
contain a userid with WAPI access to the grid master. (The script will
prompt for the corresponding password when run.)

If your grid master uses a TLS/SSL certificate from a commercial CA
then set the variable 'valid_cert' to True. If your grid contains more
than 5,000 networks then set the variable 'max_results' to the
(negative of the) number of networks to return.

This script should work for NIOS 6.8 and later (WAPI 1.2 and later).
(It depends on the new format for extensible attributes in WAPI 1.2.)
"""


# Import the required Python modules.
import requests
import json
import csv
import getpass
import sys


# Set parameters to access the Infoblox API for your own grid master.
url = 'https://gm.fhecker.com/wapi/v1.2/'
id = 'api'  # Userid with WAPI access
valid_cert = False  # True if GM uses certificate from commercial CA

# Prompt for the API user password.
pw = getpass.getpass('Password for user ' + id + ': ')


# Retrieve all network objects (up to a max of 5000). Return the
# extensible attribute values (if any) in addition to the standard
# fields.
network_view = 'default'
max_results = -5000
req_params = {'network_view': network_view,
              '_max_results': str(max_results),
              '_return_fields+': 'extattrs'}

r = requests.get(url + 'network',
                 params=req_params,
                 auth=(id, pw),
                 verify=valid_cert)

# Verify that the request succeeded. If not print error info and exit.
if r.status_code != requests.codes.ok:
    print r.text
    exit_msg = 'Error {} finding networks: {}'
    sys.exit(exit_msg.format(r.status_code, r.reason))

# Record the results and save the authentication cookie for future use.
networks = r.json()
ibapauth_cookie = r.cookies['ibapauth']

# For each network count all used addresses (up to a max of 10000)
# and store utilization statistics along with other network info.
stats = []
for network in networks:
    # Retrieve used ipv4address objects for this network.
    # Use ibapauth cookie to authenticate instead of userid/password.
    req_params={'network': network['network'],
                'network_view': network['network_view'],
                'status': 'USED',
                '_return_fields': '',
                '_max_results': str(10000)}
    req_cookies = {'ibapauth': ibapauth_cookie}

    r = requests.get(url + 'ipv4address',
                     params=req_params,
                     cookies=req_cookies,
                     verify=valid_cert)

    if r.status_code != requests.codes.ok:
        print r.text
        exit_msg = 'Error {} finding ipv4address objects: {}'
        sys.exit(exit_msg.format(r.status_code, r.reason))

    # Count the number of ipv4address objects and save the count.
    ipv4addresses = r.json()
    used = len(ipv4addresses)

    # Convert the network CIDR into network address and mask.
    netaddr, prefix = network['network'].split('/')
    prefix = int(prefix)
    netmask = '.'.join([str((0xffffffff << (32 - prefix) >> i) & 0xff)
                        for i in [24, 16, 8, 0]])

    # Calculate the percentage utilization for the network, ignoring
    # the two non-assignable addresses (network and broadcast).
    size = 2**(32 - prefix) - 2
    pct_used = round((100. * used) / size, 1)

    # Get values of the desired extensible attributes, if present.
    country = state = ''
    if 'Country' in network['extattrs']:
        country = network['extattrs']['Country']['value']
    if 'State' in network['extattrs']:
        state = network['extattrs']['State']['value']


    # Append desired info as a tuple to stats list for easy output.
    stats.append((netaddr,
                  netmask,
                  size,
                  used,
                  pct_used,
                  country,
                  state))


# Export the results.
with open('ipam-stats.csv', 'wb') as out_file:
    out_csv = csv.writer(out_file,
                         delimiter=',',
                         quotechar='"',
                         quoting=csv.QUOTE_MINIMAL)

    # Header row with field names.
    out_csv.writerow(['Network',
                      'Netmask',
                      'Size',
                      'Used',
                      'Pct Used',
                      'Country',
                      'State'])

    # One data row per network.
    for item in stats:
        out_csv.writerow(list(item))
