import requests
import sys
import json

# Suppress warning message for an invalid certificate.
requests.packages.urllib3.disable_warnings()

data_to_send = {'name': 'linux-13.test11.com',
                'ipv4addrs': [{'ipv4addr': '11.11.11.13'}],
                'view': 'default.cpview11',
                'extattrs': {'Tenant ID': {'value': 'QA'},
                             'CMP Type': {'value': 'Openstack'},
                             'Cloud API Owned': {'value':'True' },
                             'VM ID': {'value': 'VM-ID-13'},
                             'VM Name': {'value': 'LinuxVM-13'}}}


try:
    r = requests.post('https://172.26.1.11/wapi/v2.0/record:host',
                      data=json.dumps(data_to_send),
                      auth=('infobloxse', 'infobloxse'), verify=False)
except requests.Timeout:
    sys.exit('timeout creating host')

if r.status_code != requests.codes.created:
    print r.text
    errmsg = 'Error {} creating host: {}'
    sys.exit(errmsg.format(r.status_code, r.reason))

host_ref = r.json()
print host_ref
