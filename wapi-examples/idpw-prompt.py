# idpw-promot.py: Prompt for a userid and password and return tuple.

import getpass
import sys

def prompt_for_idpw(msg):
    if msg:
        print msg
    default_id = getpass.getuser()
    id = default_id
    if sys.stdin.isatty():
        pw = getpass.getpass('Using getpass: ')
    else:
        print 'Using readline'
        p = sys.stdin.readline().rstrip()
    return (id, pw)

if = '__main__':
  idpw = prompt_for_idpw('Enter Infoblox API userid and password')
  print idpw
