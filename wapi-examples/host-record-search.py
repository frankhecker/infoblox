#!/usr/local/bin/python

"""
Retrieve host record(s) matching a given name.

To use this script change the 'url' variable to contain the domain
name or IP address of the grid master, and change the 'id' variable to
contain a userid with WAPI access to the grid master. (The script will
prompt for the corresponding password when run.)

If your grid master uses a CA-issued TLS/SSL certificate then set
the variable 'valid_cert' to True for added security.

This script should work for NIOS 6.6 and later (WAPI 1.0 and later).
"""

# Import the required Python modules.
import requests
import getpass
import sys

# Set parameters to access the NIOS WAPI.
url = 'https://gm.example.com/wapi/v1.0/'
id = 'api'  # Userid with WAPI access
valid_cert = False  # True if GM uses a CA-issued certificate

# Prompt for the WAPI user password.
pw = getpass.getpass('Password for user ' + id + ': ')

# Retrieve host records matching a search string.
search_str = 'example'
r = requests.get(url + 'record:host' + '?name~=' + search_str,
                 auth=(id, pw),
                 verify=valid_cert)
if r.status_code != requests.codes.ok:
    print r.text
    exit_msg = 'Error {} finding host(s): {}'
    sys.exit(exit_msg.format(r.status_code, r.reason))
results = r.json()

# Print the raw output for each host found.
for result in results:
    print result
