#!/usr/bin/python
# Create an Infoblox host record with associated DHCP fixed address.

"""
Create a host record in a network with associated DHCP fixed address.

To use this script change the first arguments to requests.get and
requests.post to specify the domain name or IP address of the Grid
Master, and change the third argument to specify a userid and password
for the Grid Manager.

This script should work on NIOS 6.7 and later (WAPI 1.1 and later).
"""

# Import the required Python modules.
import sys
import requests
import json

# Set parameters to access the Infoblox API for your own grid master.
url = 'https://gm.lab.fhecker.com/wapi/v1.1/'
id = 'fhecker'
pw = 'xxxxxx'
valid_cert = True  # False if self-signed certificate

# FQDN, IP address, and corresponding MAC address for the host.
fqdn = 'fixed-test.lab.fhecker.com'
ip = '192.168.201.7'
mac = '01:23:45:67:89:ab'

# Create a new host record using a DHCP fixed address.
host_data = {'name': fqdn,
             'ipv4addrs': [{'ipv4addr': ip,
                            'configure_for_dhcp': True,
                            'mac': mac}]}
r = requests.post(url + 'record:host',
                  data=json.dumps(host_data),
                  auth=(id, pw),
                  verify=valid_cert)
if r.status_code != requests.codes.created:
    sys.exit('Error {} creating host: {}'.format(r.status_code, r.reason))
else:
    success_msg = 'Created host {}, IP {}, MAC {}'
    print success_msg.format(fqdn, ip, mac)
