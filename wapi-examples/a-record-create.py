#!/usr/local/bin/python

"""
Create A record at a specific address and next available address.

To use this script change the 'url' variable to contain the domain
name or IP address of a grid member supporting WAPI modify access
(grid master or cloud platform appliance), and change the 'id'
variable to contain a userid with WAPI access and the needed
permissions. (The script will prompt for the password when run.)

If the WAPI grid member uses a self-signed TLS/SSL certificate then
set the variable 'valid_cert' to False.

This script should work for NIOS 6.8.2 and later (WAPI 1.2.1 and
later).
"""

# Import the required Python modules.
import requests
import json
import getpass
import sys

# Set parameters to access the NIOS WAPI.
url = 'https://gm.example.com/wapi/v2.1/'
id = 'api'
valid_cert = False  # True if grid member has valid certificate

# Prompt for the API user password.
pw = getpass.getpass('Password for user ' + id + ': ')

# Create an A record at a specific IP address.
fqdn = 'test1.example.com'
dns_view = 'internal'
address = '192.168.0.15'
req_data = {'name': fqdn,
            'ipv4addr': address,
            'view': dns_view}
r = requests.post(url + 'record:a',
                  data=json.dumps(req_data),
                  auth=(id, pw),
                  verify=valid_cert)
if r.status_code != requests.codes.created:
    print r.text
    exit_msg = 'Error {} creating A record: {}'
    sys.exit(exit_msg.format(r.status_code, r.reason))
results = r.json()
print 'Results: ', results
print 'Created A record "' + fqdn + \
      '" pointing to "' + address + '"'

# Create an A record at the next available address, return address.
# Note 1: This requires WAPI 1.2.1 or later (NIOS 6.8.2 or later).
# Note 2: We reauthenticate using id/pw, but could have used cookie.
fqdn = 'test2.example.com'
dns_view = 'internal'
address = 'func:nextavailableip:192.168.0.0/24'
req_data = {'name': fqdn,
            'ipv4addr': address,
            'view': dns_view}
req_params = {'_return_fields': 'ipv4addr'}
r = requests.post(url + 'record:a',
                  params = req_params,
                  data=json.dumps(req_data),
                  auth=(id, pw),
                  verify=valid_cert)
if r.status_code != requests.codes.created:
    print r.text
    exit_msg = 'Error {} creating A record at available address: {}'
    sys.exit(exit_msg.format(r.status_code, r.reason))
results = r.json()
print 'Results: ', results
print 'Created A record "' + fqdn + \
      '" pointing to "' + results['ipv4addr'] + '"'
