#!/usr/local/bin/python

"""
Retrieve a network and all of its parent networks.

To use this script change the 'url' variable to contain the domain
name or IP address of the grid master, and change the 'id' variable to
contain a userid with WAPI access to the grid master. (The script will
prompt for the corresponding password when run.)

If your grid master uses a CA-issued TLS/SSL certificate then set
the variable 'valid_cert' to True for added security.

This script should work for NIOS 6.6 and later (WAPI 1.0 and later).
"""

# Import the required Python modules.
import requests
import getpass
import sys

# Set parameters to access the NIOS WAPI.
url = 'https://gm.example.com/wapi/v1.0/'
id = 'api'  # Userid with WAPI access
valid_cert = False  # True if GM uses a CA-issued certificate

# Prompt for the WAPI user password.
pw = getpass.getpass('Password for user ' + id + ': ')

# Try to find a network, assuming we don't know its CIDR prefix.
# We specify the network view to guarantee a unique result.
search_str = '10.0.0.0'
network_view = 'default'
r = requests.get(url + 'network' +
                 '?network~=' + search_str +
                 '&network_view=' + network_view +
                 '&_return_fields%2b=network_container',
                 auth=(id, pw),
                 verify=valid_cert)
if r.status_code != requests.codes.ok:
    print r.text
    exit_msg = 'Error {} finding network: {}'
    sys.exit(exit_msg.format(r.status_code, r.reason))
results = r.json()

# If we found a network, look for any containing networks.
network_hierarchy = []
look_for_parent = False
if len(results) > 0:
    result = results[0]  # get returns a list, we need the first item
    network_hierarchy.append(result['_ref'])
    look_for_parent = result['network_container'] != '/'

while look_for_parent:
    r = requests.get(url + 'networkcontainer' +
                     '?network=' + result['network_container'] +
                     '&network_view=' + network_view +
                     '&_return_fields%2b=network_container',
                     auth=(id, pw),
                     verify=valid_cert)
    if r.status_code != requests.codes.ok:
        print r.text
        exit_msg = 'Error {} finding network parent: {}'
        sys.exit(exit_msg.format(r.status_code, r.reason))
    results = r.json()
    if len(results) > 0:
        result = results[0]  # get returns a list, we need the first item
        network_hierarchy.append(result['_ref'])
        look_for_parent = result['network_container'] != '/'
    else:
        look_for_parent = False

# Print the object references in the network hierarchy.
for network_ref in network_hierarchy:
    print network_ref
